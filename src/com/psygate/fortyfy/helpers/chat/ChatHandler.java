package com.psygate.fortyfy.helpers.chat;

import org.bukkit.command.CommandSender;

public class ChatHandler {
	public static void sendMessage(MessageType type, CommandSender player, String message) {
		switch(type) {
		case ERROR:
			error(player, message);
			break;
		case INFO:
			info(player, message);
			break;
		case NOTIFY:
			notify(player, message);
			break;
		default:
			sendMessage(player, Color.White, message);
			break;
		}
	}
	
	public static void sendMessage(CommandSender player, Color color, String message) {
		player.sendMessage(color.getString()+message+color.getString());
	}
	
	public static void error(CommandSender player, String message) {
		player.sendMessage(Color.Red.getString()+message+Color.Red.getString());
	}
	
	public static void info(CommandSender player, String message) {
		player.sendMessage(Color.Gold.getString()+message+Color.Gold.getString());
	}
	
	public static void notify(CommandSender player, String message) {
		player.sendMessage(Color.Bright_Green.getString()+message+Color.Bright_Green.getString());
	}
}
