package com.psygate.fortyfy.helpers.chat;

public enum Color {
	Black("§0"),
	Dark_Blue("§1"),
	Dark_Green("§2"),
	Dark_Aqua("§3"),
	Dark_red("§4"),
	Dark_Purple("§5"),
	Gold("§6"),
	Gray("§7"),
	Dark_Gray("§8"),
	Indigo("§9"),
	Bright_Green("§a"),
	Aqua("§b"),
	Red("§c"),
	Pink("§d"),
	Yellow("§e"),
	White("§f");
	
	private String colorstring;
	private Color(String col) {
		this.colorstring = col;
	}
	
	public String getString() {
		return colorstring;
	}
}
