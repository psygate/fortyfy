package com.psygate.fortyfy.helpers.chat;

public enum MessageType {
	ERROR, NOTIFY, INFO;
}
