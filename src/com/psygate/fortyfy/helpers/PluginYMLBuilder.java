package com.psygate.fortyfy.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;

public class PluginYMLBuilder {
	public static void main(String[] args) throws Exception {
		Fortyfy fy = new Fortyfy();
		Logger.getLogger(Fortyfy.LOGGERNAME).setLevel(Level.SEVERE);
		HashMap<String, ACommand> coms = new HashMap<String, ACommand>();
		fy.registerCommands(coms);
		ArrayList<String> aliases = new ArrayList<String>();
		for(ACommand com : coms.values()) {
			if(!com.getCommandString().startsWith("fortyfy")) {
				throw new Exception("Command "+com.getCommandString()+" does not start with fortyfy.");
			}
			for(String alias : com.getAliases()) {
				if(aliases.contains(alias)) {
					throw new Exception("Alias " + alias + " is already taken.");
				} else if(alias.startsWith("/")) {
					throw new Exception("Alias " + alias +" is invalid.");
				}
			}
		}	
		
		DumperOptions options = new DumperOptions();
	    options.setPrettyFlow(true);
	    options.setExplicitStart(false);
	    options.setIndent(2);
	    options.setDefaultFlowStyle(FlowStyle.BLOCK);
		Yaml yaml = new Yaml(options);
		LinkedHashMap<String, Object> stats = new LinkedHashMap<String, Object>();
		stats.put("name", fy.getPluginName());
		stats.put("version", fy.getPluginVersion());
		stats.put("description", fy.getPluginDescription());
		stats.put("author", fy.getPluginAuthor());
		stats.put("database", fy.isPluginDatabase());
		stats.put("main", fy.getClass().getName());
		LinkedHashMap<String, Object> commands = new LinkedHashMap<String, Object>();
		stats.put("commands", commands);
		for(ACommand com : coms.values()) {
			LinkedHashMap<String, String> comdesc = new LinkedHashMap<String, String>();
			commands.put(com.getCommandString(), comdesc);
			String alias = "";
			for(int i = 0; i < com.getAliases().length; i++) {
				alias += com.getAliases()[i];
				if(i < com.getAliases().length - 1) alias += " ";
			}
			comdesc.put("aliases", alias);
			comdesc.put("description", com.getDescription());
			comdesc.put("usage", com.getUsage());
		}
		
		File plugin = new File("plugin.yml");
		plugin.delete();
		System.out.println(yaml.dump(stats));
		FileOutputStream wout = new FileOutputStream(plugin);
		PrintWriter out = new PrintWriter(wout);
		yaml.dump(stats, out);
	}
}
