package com.psygate.fortyfy.helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import com.psygate.fortyfy.Fortyfy;

public class BundleParser {
	Map<String, String> resources = new TreeMap<String, String>();
	public BundleParser() {	
		resources.put("DB.1.creategrouptable", "CREATE TABLE IF NOT EXISTS $prefix$groups(groupname VARCHAR(32) PRIMARY KEY, creator VARCHAR(16), pwp INTEGER);");
		resources.put("DB.2.creatememberable", "CREATE TABLE IF NOT EXISTS $prefix$members(name VARCHAR(16), ismod INTEGER, groupname VARCHAR(32), kicked INTEGER, FOREIGN KEY(groupname) REFERENCES $prefix$groups(groupname) ON DELETE CASCADE ON UPDATE CASCADE, UNIQUE(name, groupname));");
		resources.put("DB.3.createpasswordtable", "CREATE TABLE IF NOT EXISTS $prefix$passwords(groupname VARCHAR(32), password VARCHAR(64), FOREIGN KEY(groupname) REFERENCES $prefix$groups(groupname) ON DELETE CASCADE ON UPDATE CASCADE);");
		resources.put("DB.4.createreinforcementtable", "CREATE TABLE IF NOT EXISTS $prefix$reinforcements(x INTEGER, y INTEGER, z INTEGER, world VARCHAR(64), reinforcer VARCHAR(32), material INTEGER, totalbreaks INTEGER, remainingstrength INTEGER, mode INTEGER, materialcount INTEGER, PRIMARY KEY(x,y,z));");
		
		resources.put("DB.deletegroupleadertable", "DROP TABLE IF EXISTS $prefix$passwords;");
		resources.put("DB.deletegroupmembertable", "DROP TABLE IF EXISTS $prefix$members;");
		resources.put("DB.deletegrouptable", "DROP TABLE IF EXISTS $prefix$groups;");
		resources.put("DB.deletereinforcements", "DROP TABLE IF EXISTS $prefix$reinforcements;");
		resources.put("GroupDAO.addmember", "INSERT INTO $prefix$members VALUES(?,?,?,?);");
		resources.put("GroupDAO.creategroup", "INSERT INTO $prefix$groups VALUES(?, ?, ?);");
		resources.put("GroupDAO.creategrouppw", "INSERT INTO $prefix$groups VALUES(?, ?, ?);");
		resources.put("GroupDAO.delegategroup", "UPDATE $prefix$groups SET creator=? WHERE groupname=?;");
		resources.put("GroupDAO.dropgroup", "DELETE FROM $prefix$groups WHERE groupname=?;");
		resources.put("GroupDAO.getgroup", "SELECT creator, pwp FROM $prefix$groups WHERE groupname=?;");
		resources.put("GroupDAO.getgroupcount", "SELECT COUNT(groupname) FROM $prefix$members WHERE name=? AND kicked=?;");
		resources.put("GroupDAO.getgroupcreator", "SELECT creator FROM $prefix$groups WHERE groupname=?;");
		resources.put("GroupDAO.getgroupsofuser", "SELECT groupname FROM $prefix$members WHERE name=?;");
		resources.put("GroupDAO.getmembers", "SELECT name FROM $prefix$members WHERE groupname=? AND KICKED=?;");
		resources.put("GroupDAO.getownedgroups", "SELECT groupname FROM $prefix$groups WHERE creator=?;");
		resources.put("GroupDAO.getpassword", "SELECT password FROM $prefix$passwords WHERE groupname=?;");
		resources.put("GroupDAO.insertpw", "INSERT INTO $prefix$passwords VALUES(?,?);");
		resources.put("GroupDAO.ismemberof", "SELECT name FROM $prefix$members WHERE groupname=? AND name=? AND kicked=?;");
		resources.put("GroupDAO.ismoderator", "SELECT ismod, kicked FORM $prefix$members WHERE groupname=? AND name=?;");
		resources.put("GroupDAO.ispasswordprotected", "SELECT pwp FROM $prefix$groups WHERE groupname=?;");
		resources.put("GroupDAO.kickmember", "UPDATE $prefix$members SET kicked=?, ismod=? WHERE groupname=? AND name=?;");
		resources.put("GroupDAO.removemember", "DELETE FROM $prefix$members WHERE name=? AND groupname=?;");
		resources.put("GroupDAO.removepassword", "DELETE FROM $prefix$passwords WHERE groupname=?;");
		resources.put("GroupDAO.renamegroup", "UPDATE $prefix$groups SET groupname=? WHERE groupname=?;");
		resources.put("GroupDAO.setpassword", "INSERT INTO $prefix$passwords VALUES(?, ?);");
		resources.put("GroupDAO.updatepassword", "UPDATE $prefix$passwords SET password=? WHERE groupname=?;");
		resources.put("GroupDAO.updatepwp", "UPDATE $prefix$groups SET pwp=? WHERE groupname=?;");
		
		resources.put("ReinforcementDAO.getallreinforced", "SELECT x,y,z,world FROM $prefix$reinforcements;");
		resources.put("ReinforcementDAO.getreinforcementmode", "SELECT mode FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.isreinforced", "SELECT x,y,z,world FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.reinforce", "INSERT INTO $prefix$reinforcements VALUES (?,?,?, ?,?,?, ?,?,?,?);");
		resources.put("ReinforcementDAO.reinforcedfor", "SELECT reinforcer FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.reinforcedwith", "SELECT material, remainingstrength, totalbreaks FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.reinforcementcount", "SELECT materialcount FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.removereinforcement", "DELETE FROM $prefix$reinforcements WHERE x=? AND y=? AND z=? AND world=?;");
		resources.put("ReinforcementDAO.updatestrength", "UPDATE $prefix$reinforcements SET remainingstrength=? WHERE x=? AND y=? AND z=? AND world=?;");
	}
	public Set<String> getKeys() {		
		return resources.keySet();
	}

	public String getString(String key) {
		return resources.get(key);
	}
}
