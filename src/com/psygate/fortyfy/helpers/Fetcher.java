package com.psygate.fortyfy.helpers;

import java.io.IOException;
import java.io.InputStream;

public class Fetcher {
	public static String fetch(Object cl, String ressource) throws IOException {
		StringBuilder builder = new StringBuilder();
		InputStream in = cl.getClass().getResourceAsStream(ressource);
		
		int read;
		
		while((read = in.read()) != -1) {
			builder.append((char)read);
		}
		
		in.close();
		return builder.toString();
	}
}
