package com.psygate.fortyfy.helpers;

import java.io.File;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class BundleBuilder {
	public static void main(String[] args) throws Exception	{
		ResourceBundle bundle = ResourceBundle.getBundle("com.psygate.fortyfy.database.db");
		Enumeration<String> keys = bundle.getKeys();
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("package com.psygate.fortyfy.helpers;\n");

		builder.append("import java.io.IOException;\n");
		builder.append("import java.io.InputStream;\n");
		builder.append("import java.net.MalformedURLException;\n");
		builder.append("import java.net.URL;\n");
		builder.append("import java.util.Enumeration;\n");
		builder.append("import java.util.HashMap;\n");
		builder.append("import java.util.Iterator;\n");
		builder.append("import java.util.ResourceBundle;\n");
		builder.append("import java.util.Set;\n");
		builder.append("import java.util.logging.Level;\n");
		builder.append("import java.util.logging.Logger;\n");

		builder.append("import com.psygate.fortyfy.Fortyfy;\n");

		builder.append("public class BundleParser {\n");
		builder.append("	private Logger logger = Logger.getLogger(Fortyfy.LOGGERNAME);\n");
		
		builder.append("	HashMap<String, String> resources = new HashMap<String, String>();\n");
		builder.append("	public BundleParser() {");
		Map<String, String> vals = new TreeMap<String, String>();
		while(keys.hasMoreElements()) {
			String key = keys.nextElement();
			vals.put(key, bundle.getString(key));
		}
		
		for(String key : vals.keySet()) {
			builder.append("\t\tresources.put(\""+key+"\", \""+vals.get(key)+"\");\n");
		}
		
		builder.append("	}\n");
		
		builder.append("	public Set<String> getKeys() {");
		builder.append("		return resources.keySet();\n");
		builder.append("	}\n");
		builder.append("\n");
		builder.append("	public String getString(String key) {\n");
		builder.append("		return resources.get(key);\n");
		builder.append("	}\n");
		
		builder.append("}\n");
		
		File target = new File("src/com/psygate/fortyfy/helpers/BundleParser.java");
		System.out.println(builder.toString());
		PrintWriter out = new PrintWriter(target);
		out.write(builder.toString());
		out.close();
	}
}
