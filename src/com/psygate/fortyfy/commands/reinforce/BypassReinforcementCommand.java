package com.psygate.fortyfy.commands.reinforce;

import static com.psygate.fortyfy.helpers.chat.MessageType.INFO;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class BypassReinforcementCommand extends ACommand {
	public BypassReinforcementCommand() {
		super("fortyfybypassreinforcement", "Bypass reinforcements.", new String[] {"fb"}, "/fb");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		fortyfy.getPlayerStateHolder().setBypassing(sender.getName());
		return new Vector2<MessageType, String>(INFO, "Bypassing reinforcements.");
	}
}
