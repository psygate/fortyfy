package com.psygate.fortyfy.commands.reinforce;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class PublicReinforceCommand extends ACommand {
	public PublicReinforceCommand() {
		super(
				"fortyfyreinforcepublic",
				"Reinforces blocks you place publicly with the reinforcement material.",
				new String[] { "frp" }, "/frp");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		Player player = fortyfy.getServer().getPlayer(sender.getName());
		if (player == null || !player.isOnline()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must be a player and online to use that command.");
		}

		ItemStack hand = player.getItemInHand();
		if (fortyfy.getReinforcements().isFortification(hand.getType())) {
			fortyfy.getPlayerStateHolder().setPlayerState(player.getName(),
					hand.getType(), ReinforcementMode.PUBLIC);
			return new Vector2<MessageType, String>(MessageType.INFO,
					"Single block reinforcement with " + hand.getType().name());
		} else {
			return new Vector2<MessageType, String>(MessageType.ERROR, hand
					.getType().name()
					+ " is not a valid reinforcement material.");
		}
	}
}
