package com.psygate.fortyfy.commands.reinforce;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;
import static com.psygate.fortyfy.helpers.chat.MessageType.*;
import static com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode.*;

public class PlaceReinforceCommand extends ACommand {
	public PlaceReinforceCommand() {
		super("fortyfyreinforce",
				"Reinforces blocks you place with the reinforcement material.",
				new String[] { "fr" }, "/fr");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {

		if (!fortyfy.getServer().getPlayer(sender.getName()).isOnline()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"Only online players may use this command.");
		}

		Material mat = fortyfy.getServer().getPlayer(sender.getName())
				.getItemInHand().getType();
		if (!fortyfy.getReinforcements().isFortification(mat)) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					mat.name() + " is not a valid reinforcement.");
		}

		if (args.length == 0) {
			return singleReinforce(fortyfy, sender);
		} else if (args.length >= 1) {
			String mode = args[0].toLowerCase();
			if ("public".equals(mode)) {
				return publicReinforce(fortyfy, sender);
			} else if ("private".equals(mode)) {
				return singleReinforce(fortyfy, sender);
			} else if ("group".equals(mode)) {
				if (args.length < 2)
					return new Vector2<MessageType, String>(ERROR,
							"You must provide a groupname.");
				String groupname = args[1];
				if (!fortyfy.getGroupDAO().isGroupExistent(groupname)) {
					return new Vector2<MessageType, String>(ERROR,
							"You cannot reinforce for groups which do not exist.");
				} else if (!fortyfy.getGroupDAO().isMemberOf(groupname,
						sender.getName())) {
					return new Vector2<MessageType, String>(ERROR,
							"You cannot reinforce for groups which you are not a member of.");
				}

				return groupReinforce(fortyfy, sender, groupname);
			}
		}
		
		return new Vector2<MessageType, String>(ERROR,
				args[0] + " is not a valid mode.");
	}

	private Vector2<MessageType, String> singleReinforce(Fortyfy fortyfy,
			CommandSender sender) {
		Player player = fortyfy.getServer().getPlayer(sender.getName());
		ItemStack hand = player.getItemInHand();
		fortyfy.getPlayerStateHolder().setPlayerState(player.getName(),
				hand.getType(), ReinforcementMode.PRIVATE);
		return new Vector2<MessageType, String>(MessageType.INFO,
				"Private block reinforcement with " + hand.getType().name());
	}

	private Vector2<MessageType, String> groupReinforce(Fortyfy fortyfy,
			CommandSender sender, String groupname) {
		Player player = fortyfy.getServer().getPlayer(sender.getName());
		ItemStack hand = player.getItemInHand();
		fortyfy.getPlayerStateHolder().setPlayerState(sender.getName(),
				hand.getType(), GROUP, groupname);
		return new Vector2<MessageType, String>(MessageType.INFO,
				"Group block reinforcement with " + hand.getType().name()
						+ " for " + groupname);
	}

	private Vector2<MessageType, String> publicReinforce(Fortyfy fortyfy,
			CommandSender sender) {
		Player player = fortyfy.getServer().getPlayer(sender.getName());
		ItemStack hand = player.getItemInHand();
		fortyfy.getPlayerStateHolder().setPlayerState(player.getName(),
				hand.getType(), ReinforcementMode.PUBLIC);
		return new Vector2<MessageType, String>(MessageType.INFO,
				"Public block reinforcement with " + hand.getType().name());

	}
}
