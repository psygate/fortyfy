package com.psygate.fortyfy.commands.reinforce;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;
import static com.psygate.fortyfy.helpers.chat.MessageType.*;
import static com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode.*;

public abstract class AReinforceCommand extends ACommand {
	protected AReinforceCommand(String commandstring, String description,
			String[] aliases, String usage) {
		super(commandstring, description, aliases, usage);
	}

	protected boolean isOnline(CommandSender sender) {
		if(sender == null) return false;
		Server server = sender.getServer();
		if(server == null) return false;
		Player player = server.getPlayer(sender.getName());
		if(player == null) 	return false;
		return player.isOnline();
	}
	
	protected Player getPlayer(CommandSender sender) {
		if(sender == null) return null;
		if(sender.getServer() == null) return null;
		return sender.getServer().getPlayer(sender.getName());
	}
	
	protected Vector2<MessageType, String> placeReinforce(CommandSender sender, Fortyfy fy) throws Exception {
		if(!isOnline(sender)) {
			return new Vector2<MessageType, String>(ERROR, "You must be online to use this command.");
		}
		
		Player player = getPlayer(sender);
		if(player == null) {
			logger.severe("Player not found.");
			return null;
		}
		
		Material mat = player.getItemInHand().getType();
		
		if(!fy.getReinforcements().isFortification(mat)) {
			return new Vector2<MessageType, String>(ERROR, mat.name()+" is not a valid reinforcement material.");
		}
		
		fy.getPlayerStateHolder().setPlayerState(player.getName(), mat, PRIVATE);
		return new Vector2<MessageType, String>(INFO, "Reinforcing "+PRIVATE+" on placement with "+mat.name());
	}
	
	protected Vector2<MessageType, String> placeGroupReinforce(CommandSender sender, Fortyfy fy, String[] args) throws Exception {
		if(!isOnline(sender)) {
			return new Vector2<MessageType, String>(ERROR, "You must be online to use this command.");
		}
		
		Player player = getPlayer(sender);
		if(player == null) {
			logger.severe("Player not found.");
			return null;
		}
		
		Material mat = player.getItemInHand().getType();
		if(args.length < 2) {
			return new Vector2<MessageType, String>(ERROR, "You must provide a groupname to reinforce for.");
		} else if(!fy.getGroupDAO().isGroupExistent(args[1])) {
			return new Vector2<MessageType, String>(ERROR, "You cannot reinforce for an inexistent group.");
		} else {
			if(!fy.getReinforcements().isFortification(mat)) {
				return new Vector2<MessageType, String>(ERROR, mat.name()+" is not a valid reinforcement material.");
			}
			
			fy.getPlayerStateHolder().setPlayerState(player.getName(), mat, GROUP, args[1]);
			return new Vector2<MessageType, String>(INFO, "Reinforcing "+GROUP+"("+args[1]+") on placement with "+mat.name());
		}
	}
	
	protected Vector2<MessageType, String> placePublicreinforce(CommandSender sender, Fortyfy fy) {
		if(!isOnline(sender)) {
			return new Vector2<MessageType, String>(ERROR, "You must be online to use this command.");
		}
		
		Player player = getPlayer(sender);
		if(player == null) {
			logger.severe("Player not found.");
			return null;
		}
		
		Material mat = player.getItemInHand().getType();
		
		if(!fy.getReinforcements().isFortification(mat)) {
			return new Vector2<MessageType, String>(ERROR, mat.name()+" is not a valid reinforcement material.");
		}
		
		fy.getPlayerStateHolder().setPlayerState(player.getName(), mat, PUBLIC);
		return new Vector2<MessageType, String>(INFO, "Reinforcing "+PUBLIC+" on placement with "+mat.name());
	}
}
