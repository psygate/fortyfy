package com.psygate.fortyfy.commands.reinforce;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class GroupReinforceCommand extends ACommand {
	public GroupReinforceCommand() {
		super("fortyfyreinforcegroup", "Reinforces blocks you place in groupmode.", new String[] {"frg"}, "/frg <groupname>");
	}
	
	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		Player player = fortyfy.getServer().getPlayer(sender.getName());
		if(player == null || !player.isOnline()) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You must be a player and online to use that command.");
		}
		
		if(args.length < 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You must provide a group to reinforce for.");
		}
		
		ItemStack hand = player.getItemInHand();
		if(fortyfy.getReinforcements().isFortification(hand.getType())) {
			if(!fortyfy.getGroupDAO().isGroupExistent(args[0])) {
				return new Vector2<MessageType, String>(MessageType.ERROR, "Group "+args[0]+" does not exist.");
			}
			
			fortyfy.getPlayerStateHolder().setPlayerState(player.getName(), hand.getType(), ReinforcementMode.GROUP, args[0]);
			return new Vector2<MessageType, String>(MessageType.INFO, "Group block reinforcement with "+hand.getType().name());
			
		} else {
			return new Vector2<MessageType, String>(MessageType.ERROR, hand.getType().name()+" is not a valid reinforcement material.");
		}
	}
}
