package com.psygate.fortyfy.commands.reinforce;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class ReinforcementInfoCommand extends ACommand {

	public ReinforcementInfoCommand() {
		super("fortyfyreinforcementinfo", "Provides on click information about reinforcements.", new String[]{"fi"}, "/fi");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		fortyfy.getPlayerStateHolder().setInformationMode(sender.getName());
		return new Vector2<MessageType, String>(MessageType.INFO, "INFO mode started.");
	}

}
