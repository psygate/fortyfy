package com.psygate.fortyfy.commands;

import java.util.logging.Logger;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public abstract class ACommand {
	private String PERMISSION = "com.psygate.fortyfy.command";
	private String COMSTRING = "ACommand";
	private String DESCRIPTION = "No description available.";
	private String[] ALIASES = new String[] {"acommand"};
	private String USAGE = "No usage.";
	
	public ACommand(String commandstring, String description, String[] aliases, String usage) {
		this.PERMISSION = this.getClass().getName();
		this.COMSTRING = commandstring;
		this.DESCRIPTION = description;
		this.ALIASES = aliases;
		this.USAGE = usage;
	}
	
	protected Logger logger = Logger.getLogger(Fortyfy.LOGGERNAME);
	public abstract Vector2<MessageType, String> execute(CommandSender sender, Command command, String label, String[] args, Database db, Fortyfy fortyfy) throws Exception;
	
	public String getCommandString() {
		return COMSTRING;
	}
	
	public String getPermissionString() {
		return PERMISSION;
	}
	
	public String getDescription() {
		return DESCRIPTION;
	}
	
	public String[] getAliases() {
		return ALIASES;
	}
	
	public String getUsage() {
		return USAGE;
	}
}
