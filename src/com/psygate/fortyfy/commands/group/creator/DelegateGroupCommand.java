package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class DelegateGroupCommand extends ACommand {

	public DelegateGroupCommand() {
		super("fortyfydelegategroup", "Moves the group to another user.", new String[] {"fgm"}, "/fgm <groupname> <username>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {

		if (args.length == 0) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must atleast provide a groupname.");
		} else if (args.length > 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You can only move 1 group at a time.");
		} else if(args.length == 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must provide someone to delegate the group to.");
		}

		if (!fortyfy.getGroupDAO().isGroupExistent(args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"The group you specified does not exist.");
		} else if (!fortyfy.getGroupDAO().isCreator(sender.getName(), args[0]) && !sender.isOp()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You cannot delegate the leadership of a group you are not the leader of.");
		}

		fortyfy.getGroupDAO().moveGroup(args[0], args[1]);
		return new Vector2<MessageType, String>(MessageType.INFO, "Group "
				+ args[0] + " has a new leader: " + args[1]);

	}
}
