package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class RenameGroupCommand extends ACommand {

	public RenameGroupCommand() {
		super("fortyfygrouprename",
				"Renames a group. You must be creator of that group.",
				new String[] { "fgr" }, "/fgr <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {

		if (args.length == 0) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must atleast provide a groupname.");
		} else if (args.length > 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You can only rename 1 group at a time.");
		} else if (args.length < 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must provide the old AND new group name.");
		}

		if (!fortyfy.getGroupDAO().isGroupExistent(args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"The group you specified does not exist.");
		} else if (!fortyfy.getGroupDAO().isCreator(args[1], args[0]) && !sender.isOp()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You cannot rename a group you are not a leader of.");
		} else if (fortyfy.getGroupDAO().isGroupExistent(args[1])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"The new groupname already exists.");
		}

		fortyfy.getGroupDAO().renameGroup(args[0], args[1]);
		return new Vector2<MessageType, String>(MessageType.INFO, "Group "
				+ args[0] + " is now known as " + args[1]);
	}
}
