package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class DeleteGroupCommand extends ACommand {

	public DeleteGroupCommand() {
		super("fortyfygroupdelete", "Deletes a group, you must own that group.", new String[] {"fgd"}, "/fgd <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {

		if (args.length < 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must specify a group you want to delete.");
		} else if (args.length > 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You cannot delete more than one group at the same time.");
		}

		if (!fortyfy.getGroupDAO().isCreator(sender.getName(), args[0]) && !sender.isOp()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"Only the creator of a group may delete it.");
		} else {
			fortyfy.getGroupDAO().deleteGroup(args[0]);
			return new Vector2<MessageType, String>(MessageType.INFO, "Group "
					+ args[0] + " has been deleted.");
		}
	}
}
