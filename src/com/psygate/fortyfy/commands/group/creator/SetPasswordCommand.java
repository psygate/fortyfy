package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class SetPasswordCommand extends ACommand {
	public SetPasswordCommand() {
		super("fortyfygrouppasswordset", "Removes the password from a group. You must be creator of that group.", new String[] {"fgps"}, "/fgps <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		if (args.length < 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You need to provide a group and a password to set.");
		}

		if (!fortyfy.getGroupDAO().isCreator(sender.getName(), args[0]) && !sender.isOp()) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"Only the creator can add a password to a group.");
		} else {
			fortyfy.getGroupDAO().setPassword(args[0], args[1]);
			return new Vector2<MessageType, String>(MessageType.INFO,
					"Password " + args[1] + " has been set for group "
							+ args[0]);
		}
	}
}
