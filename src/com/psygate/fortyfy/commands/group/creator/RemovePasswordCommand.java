package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class RemovePasswordCommand extends ACommand {

	public RemovePasswordCommand() {
		super("fortyfygrouppasswordremove",
				"Removes passwordprotection from a group.",
				new String[] { "fgpr" }, "/fgpr <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		// TODO Auto-generated method stub
		if (args.length < 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You need to provide a group.");
		}

		if (!fortyfy.getGroupDAO().isCreator(sender.getName(), args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"Only the creator can remove a password from a group.");
		} else {
			fortyfy.getGroupDAO().removePassword(args[0]);
			return new Vector2<MessageType, String>(MessageType.INFO,
					"Password has been removed from group " + args[0]);
		}
	}
}
