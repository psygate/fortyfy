package com.psygate.fortyfy.commands.group.creator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class CreateGroupCommand extends ACommand {
	public CreateGroupCommand() {
		super("fortyfygroupcreate", "Creates a group for reinforcement usage.", new String[] {"fgc"}, "/fgc <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender, Command command, String label,
			String[] args, Database db, Fortyfy fortyfy) throws Exception {

		if (args.length == 0) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You must atleast provide a groupname.");
		} else if (args.length > 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You can only create 1 group at a time.");
		}

		if (fortyfy.getGroupDAO().isGroupExistent(args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "The group you specified already exists.");
		}

		if(fortyfy.getGroupDAO().getGroupCount(sender.getName()) > fortyfy.getMaxGroupCount()) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You already are leader of the maximum amount of groups.");
		}
		
		if (args.length == 2) {
			fortyfy.getGroupDAO().createGroupWithPassword(sender.getName(),
					args[0], args[1]);
		} else {
			fortyfy.getGroupDAO().createGroup(sender.getName(), args[0]);
		}
		return new Vector2<MessageType, String>(MessageType.INFO, "Group "+args[0]+ " has been created.");
	}
}
