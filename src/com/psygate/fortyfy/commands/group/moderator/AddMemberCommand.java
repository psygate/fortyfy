package com.psygate.fortyfy.commands.group.moderator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class AddMemberCommand extends ACommand {
	public AddMemberCommand() {
		super("fortyfygroupmemberadd", "Adds a member to a group.", new String[] {"fgma"}, "/fgma <groupname> <membername>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		if (args.length < 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must provide a groupname and a new member name.");
		}

		if (!fortyfy.getGroupDAO().isModerator(args[0], sender.getName())) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"Only moderators are allowed to do that.");
		} else {
			fortyfy.getGroupDAO().addMember(args[0], args[1]);
			return new Vector2<MessageType, String>(MessageType.INFO,
					"Player " + args[1] + " has been added to group " + args[0]);
		}
	}
}
