package com.psygate.fortyfy.commands.group.moderator;

import java.util.Collection;
import java.util.Iterator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class ListMembersCommand extends ACommand {

	public ListMembersCommand() {
		super("fortyfygrouplistmembers", "Lists all members of the provided group.", new String[] {"fglm"}, "/fglm <groupname>");
		// TODO Auto-generated constructor stub
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		if(args.length > 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You can only request members of one group at a time.");
		} else if(args.length < 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR, "You must provide a group name.");
		}
		
		Collection<String> members = fortyfy.getGroupDAO().getMembers(args[0]);
		Iterator<String> it = members.iterator();
		StringBuilder out = new StringBuilder();
		int cntr = 0;
		while(it.hasNext()) {
			cntr++;
			out.append(it.next());
			if(cntr < members.size()) {
				out.append(", ");
			}
		}
		
		return new Vector2<MessageType, String>(MessageType.INFO, out.toString());
	}
}
