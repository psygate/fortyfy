package com.psygate.fortyfy.commands.group.member;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class JoinGroupCommand extends ACommand {

	public JoinGroupCommand() {
		super("fortyfygroupjoin", "Joins a password protected group.", new String[] {"fgj"}, "/fgj <groupname> <password>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {

		if (args.length < 2) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must provide a group and a password to join.");
		}
		
		if (!fortyfy.getGroupDAO().isGroupExistent(args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"The group you want to join does not exist.");
		} else if (!fortyfy.getGroupDAO().isPasswordProtected(args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"The group you want to join is invitation only.");
		} else if (fortyfy.getGroupDAO().isKicked(args[0], sender.getName())) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You cannot rejoin a group you have been kicked from.");
		} else {
			if(fortyfy.getGroupDAO().isMemberOf(args[1], sender.getName())) {
				return new Vector2<MessageType, String>(MessageType.INFO, "You are already a member of "+args[0]);
			}
			if (args[1] != null
					&& args[1].equals(fortyfy.getGroupDAO()
							.getPassword(args[0]))) {
				fortyfy.getGroupDAO().addMember(args[0], sender.getName());
				return new Vector2<MessageType, String>(MessageType.INFO,
						"You joined the group " + args[0]);
			} else {
				return new Vector2<MessageType, String>(MessageType.ERROR,
						"Wrong password.");
			}
		}

	}

}
