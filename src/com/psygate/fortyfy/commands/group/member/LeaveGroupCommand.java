package com.psygate.fortyfy.commands.group.member;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class LeaveGroupCommand extends ACommand {
	public LeaveGroupCommand() {
		super("fortyfygroupleave", "Leaves a group you are member of.", new String[] {"fgl"}, "/fgl <groupname>");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		if (args.length < 1) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You must provide a group you want to leave.");
		}

		if (fortyfy.getGroupDAO().isCreator(sender.getName(), args[0])) {
			return new Vector2<MessageType, String>(MessageType.ERROR,
					"You cannot leave a group which you are still a creator of.");
		} else {
			fortyfy.getGroupDAO().leaveGroup(sender.getName(), args[0]);
			return new Vector2<MessageType, String>(MessageType.INFO,
					"You have left the group " + args[0]);
		}
	}

}
