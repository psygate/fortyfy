package com.psygate.fortyfy.commands.group.member;

import java.util.Collection;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class ListOwnedGroupsCommand extends ACommand {
	public ListOwnedGroupsCommand() {
		super("fortyfygrouplistowned", "Lists the groups you are creator of.", new String[] {"fglo"}, "/fglo");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		Collection<String> groups = fortyfy.getGroupDAO().getOwnedGroups(
				sender.getName());
		StringBuilder builder = new StringBuilder("Your owned groups: ");
		int cntr = 0;
		for (String name : groups) {
			builder.append(name);
			if (cntr < groups.size()) {
				builder.append(", ");
			}
			cntr++;
		}
		return new Vector2<MessageType, String>(MessageType.INFO,
				builder.toString());
	}
}
