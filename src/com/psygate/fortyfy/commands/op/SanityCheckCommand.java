package com.psygate.fortyfy.commands.op;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import  com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.helpers.chat.MessageType;

public class SanityCheckCommand extends ACommand {
	public SanityCheckCommand() {
		super("fortyfysanitycheck", "Checks the database for insane reinforced blocks.", new String[] {"fsc"}, "/fsc");
	}

	@Override
	public Vector2<MessageType, String> execute(CommandSender sender,
			Command command, String label, String[] args, Database db,
			Fortyfy fortyfy) throws Exception {
		if(!sender.isOp()) return new Vector2<MessageType, String>(MessageType.NOTIFY, "Only an operator may do this.");
		int checked = fortyfy.getReinforcementDAO().performSanityCheck(sender, fortyfy);
		return new Vector2<MessageType, String>(MessageType.NOTIFY, "Sanity check was performed. Cleaned "+checked+" invalid blocks.");
	}

}
