package com.psygate.fortyfy.listeners;

import static com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode.GROUP;
import static com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode.PRIVATE;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class DoorOpenListener extends AListener {
	public DoorOpenListener(Fortyfy fortyfy) {
		super(fortyfy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void doorOpen(PlayerInteractEvent event) throws Exception {
		if(event.getClickedBlock() == null) return;
		if(doors.contains(event.getClickedBlock().getTypeId())) {
			Block block = delegate(event.getClickedBlock());
			String name = event.getPlayer().getName();
			if(fortyfy.getReinforcementDAO().isReinforced(block)) {
				ReinforcementMode mode = fortyfy.getReinforcementDAO().getReinforcementMode(block);
				String reinfor = fortyfy.getReinforcementDAO().getReinforcedFor(block);
				if(mode.equals(PRIVATE) && !reinfor.equals(name)) {
					event.setCancelled(true);
					if(event.getAction() == Action.RIGHT_CLICK_BLOCK)ChatHandler.error(event.getPlayer(), "Door is locked.");
				} else if(mode.equals(GROUP) && !fortyfy.getGroupDAO().isMemberOf(reinfor, name)) {
					if(event.getAction() == Action.RIGHT_CLICK_BLOCK)ChatHandler.error(event.getPlayer(), "Door is locked.");
					event.setCancelled(true);
				}
			}
		}
	}
}
