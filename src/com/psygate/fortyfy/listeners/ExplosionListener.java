package com.psygate.fortyfy.listeners;

import java.util.Iterator;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.psygate.fortyfy.Fortyfy;

public class ExplosionListener extends AListener {
	public ExplosionListener(Fortyfy fy) {
		super(fy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void entityExplode(EntityExplodeEvent event) throws Exception {
		Iterator<Block> it = event.blockList().iterator();
		while(it.hasNext()) {
			Block block = it.next();
			Block cblock = delegate(block);
			if(fortyfy.getReinforcementDAO().isReinforced(cblock)) {
				fortyfy.getReinforcementDAO().updateReinforcementStrength(cblock, fortyfy.getReinforcementDAO().getReinforcementStrength(cblock)-1);
				int strength = fortyfy.getReinforcementDAO().getReinforcementStrength(cblock);
				if(strength > 0) {
					fortyfy.getReinforcementDAO().updateReinforcementStrength(cblock, strength - 1);
					it.remove();
				} else {
					fortyfy.getReinforcementDAO().removeReinforced(cblock);
				}
			}
		}
	}
}
