package com.psygate.fortyfy.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockRedstoneEvent;

import com.psygate.fortyfy.Fortyfy;

public class RedstoneListener extends AListener {
	public RedstoneListener(Fortyfy fy) {
		super(fy);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void redstoneChange(BlockRedstoneEvent event) throws Exception {
		Block block = delegate(event.getBlock());
		if (redstonepassthrough.contains(block.getType())) {
			return;
		}
		boolean isreinforced = fortyfy.getReinforcementDAO().isReinforced(block);
		if (isreinforced) {
			logger.info("Reinforced encountered.");
			if (!isPlayerClose(event)) {
				logger.info("Redstone event prevented.");
				event.setNewCurrent(event.getOldCurrent());
			} else {
				logger.info("Allowing player is close.");
			}
		}
	}
}
