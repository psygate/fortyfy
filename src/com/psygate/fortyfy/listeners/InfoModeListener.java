package com.psygate.fortyfy.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Bed;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class InfoModeListener extends AListener {

	public InfoModeListener(Fortyfy fortyfy) {
		super(fortyfy);
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void entityPoint(PlayerInteractEvent e) throws Exception {
		Block block = e.getClickedBlock();
		if (block == null)
			return;
		block = delegate(block);
		if (fortyfy.getPlayerStateHolder().isInfoMode(e.getPlayer().getName())) {
			if (fortyfy.getReinforcementDAO().isReinforced(block)) {
				Vector3<Material, Integer, Integer> vec = fortyfy
						.getReinforcementDAO().getReinforcedWith(block);
				Material mat = vec.getA();
				int perc = (int) (((float) vec.getB() / (float) vec.getC()) * 100);
				ReinforcementMode mode = fortyfy.getReinforcementDAO()
						.getReinforcementMode(block);
				if (!e.getPlayer().isOp()) {
					String out = "Block is reinforced @" + perc + " with "
							+ mat.name() + " (" + mode + ")";
					ChatHandler.info(e.getPlayer(), out);
				} else {
					String out = "Block is reinforced @" + perc + " with "
							+ mat.name() + " (" + mode + ")";
					out += "\nbreaks: " + vec.getB() + " strength: "
							+ vec.getC();
					ChatHandler.info(e.getPlayer(), out);
				}
			}

			if (e.getPlayer().isOp())
				ChatHandler.info(
						e.getPlayer(),
						"Block: " + block.getTypeId() + " Data: "
								+ block.getData() + " Bin: "
								+ Integer.toBinaryString(block.getData()));

		}
	}
}
