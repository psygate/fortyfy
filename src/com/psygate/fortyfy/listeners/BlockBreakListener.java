package com.psygate.fortyfy.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;

public class BlockBreakListener extends AListener {
	public BlockBreakListener(Fortyfy fy) {
		super(fy);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void blockPlacement(BlockBreakEvent event) throws Exception {
		Block block = event.getBlock();
		String player = event.getPlayer().getName();
		block = delegate(block);

		if (fortyfy.getReinforcementDAO().isReinforced(block)) {
			logger.info("Block: " + block + " reinforced for "
					+ fortyfy.getReinforcementDAO().getReinforcedFor(block)
					+ " mode: "
					+ fortyfy.getReinforcementDAO().getReinforcementMode(block));
			Vector3<Material, Integer, Integer> matvec = fortyfy
					.getReinforcementDAO().getReinforcedWith(block);
			boolean isbypassing = fortyfy.getPlayerStateHolder().isBypassing(
					player);
			boolean maybypass = (isbypassing) ? fortyfy.getReinforcementDAO()
					.mayBypass(block, player) : false;

			if (isbypassing && maybypass) {
				int count = fortyfy.getReinforcementDAO().getReinforcementCount(block);
				ItemStack stack = new ItemStack(matvec.getA(), count);
				float broken = matvec.getB();
				float breaks = matvec.getC();
				if (broken / breaks > 0.5)
					block.getWorld().dropItemNaturally(block.getLocation(),
							stack);
				fortyfy.getReinforcementDAO().removeReinforced(block);
			} else if (matvec.getB() > 0) {
				fortyfy.getReinforcementDAO().updateReinforcementStrength(
						block, matvec.getB() - 1);
				event.setCancelled(true);
			} else if (matvec.getB() < 0) {
				event.setCancelled(true);
			} else if (matvec.getB() == 0) {
				fortyfy.getReinforcementDAO().removeReinforced(block);
			} else {
				throw new Exception("Unknown state encountered.");
			}
		}
	}
}
