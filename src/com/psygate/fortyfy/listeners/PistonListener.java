package com.psygate.fortyfy.listeners;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

import com.psygate.fortyfy.Fortyfy;

public class PistonListener extends AListener {
	private static final int PISTON_PUSH_LENGTH = 12;
	public PistonListener(Fortyfy fortyfy) {
		super(fortyfy);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void pistonExtendEvent(BlockPistonExtendEvent event)
			throws Exception {
		Block base = event.getBlock();
		BlockFace direction = event.getDirection();
		for(int i = 0; i < PISTON_PUSH_LENGTH; i++) {
			Block pushed = base.getRelative(direction, i + 1);
			logger.info("Checking "+pushed);
			if(pushed == null || Material.AIR.equals(pushed.getType())) break;
			if(fortyfy.getReinforcementDAO().isReinforced(delegate(pushed))) {
				event.setCancelled(true);
				break;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void pistonRetractEvent(BlockPistonRetractEvent event)
			throws Exception {
		
		if(!event.isSticky()) {
			logger.info("Piston is not sticky. Allowing retraction.");
			return;
		}
		
		Block base = event.getBlock();
		Block pulled = base.getRelative(event.getDirection()).getRelative(event.getDirection());
		
		if (fortyfy.getReinforcementDAO().isReinforced(pulled)) {
			event.setCancelled(true);
			logger.info("Piston retraction stopped. "+pulled);
			return;
		}
	}

}
