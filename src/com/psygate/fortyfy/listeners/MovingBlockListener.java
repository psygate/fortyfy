package com.psygate.fortyfy.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import com.psygate.fortyfy.Fortyfy;

public class MovingBlockListener extends AListener {

	public MovingBlockListener(Fortyfy fortyfy) {
		super(fortyfy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void falling(EntityChangeBlockEvent eve) throws Exception {
		logger.info("Entitychange "+eve);
		if(fortyfy.getReinforcementDAO().isReinforced(eve.getBlock())) {
			fortyfy.getReinforcementDAO().removeReinforced(eve.getBlock());
		}
	}
}
