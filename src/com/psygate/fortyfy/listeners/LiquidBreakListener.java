package com.psygate.fortyfy.listeners;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockFromToEvent;

import com.psygate.fortyfy.Fortyfy;

public class LiquidBreakListener extends AListener {

	public LiquidBreakListener(Fortyfy fortyfy) {
		super(fortyfy);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void liquidBreak(BlockFromToEvent event) throws Exception {
		Block to = event.getToBlock();
		if (liquidbreakable.contains(to.getType())) {
			if (fortyfy.getReinforcementDAO().isReinforced(to)) {
				event.setCancelled(true);
			}
		}
	}
}
