package com.psygate.fortyfy.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBurnEvent;

import com.psygate.fortyfy.Fortyfy;

public class FireBreakListener extends AListener {

	public FireBreakListener(Fortyfy fortyfy) {
		super(fortyfy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void fireBreak(BlockBurnEvent event) throws Exception {
		Block block = event.getBlock();
		if(fortyfy.getReinforcementDAO().isReinforced(block)) {
			int strength = fortyfy.getReinforcementDAO().getReinforcementStrength(block);
			fortyfy.getReinforcementDAO().updateReinforcementStrength(block, strength - 1);
			for(BlockFace face : sixdir) {
				if(block.getRelative(face).getType().equals(Material.FIRE)) {
					block.getRelative(face).setType(Material.AIR);
				}
			}
			event.setCancelled(true);
		}
	}

}
