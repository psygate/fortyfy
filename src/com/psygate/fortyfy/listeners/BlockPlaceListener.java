package com.psygate.fortyfy.listeners;

import static com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode.*;

import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class BlockPlaceListener extends AListener {

	public BlockPlaceListener(Fortyfy fy) {
		super(fy);
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR)
	public void blockPlacement(BlockPlaceEvent event) throws Exception {
		String playername = event.getPlayer().getName();
		if (fortyfy.getPlayerStateHolder().isReinforceingAndRefresh(playername)) {
			if (fortyfy.getReinforcements().isNonReinforceable(
					event.getBlock().getType())) {
				ChatHandler.error(event.getPlayer(), event.getBlock().getType()
						+ " is not reinforceable.");
				return;
			}

			if (fortyfy.getReinforcementDAO().isReinforced(event.getBlock())) {
				logger.warning(event.getBlock() + " is already reinforced.");
				return;
			}
			switch (fortyfy.getPlayerStateHolder().getPlayerMode(playername)) {
			case GROUP:
				reinforceGroup(event.getBlock(), playername, event.getPlayer());
				break;
			case NONE:
				break;
			case PRIVATE:
				reinforceSingle(event.getBlock(), playername, event.getPlayer());
				break;
			case PUBLIC:
				reinforcePublic(event.getBlock(), playername, event.getPlayer());
				break;
			default:
				break;
			}
			
			event.getPlayer().updateInventory();	//Deprecated, but there is no other workaround.
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void entityPoint(PlayerInteractEvent event) throws Exception {
		if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
			String playername = event.getPlayer().getName();
			if(fortyfy.getPlayerStateHolder().isReinforceingAndRefresh(playername)) {
				ReinforcementMode mode = fortyfy.getPlayerStateHolder().getPlayerMode(playername);
				if(ReinforcementMode.isClickMode(mode)) {
					Block block = event.getClickedBlock();
					if(!fortyfy.getReinforcementDAO().isReinforced(block)) {
						ReinforcementMode real = ReinforcementMode.translate(mode);
						switch (fortyfy.getPlayerStateHolder().getPlayerMode(playername)) {
						case GROUP:
							reinforceGroup(event.getClickedBlock(), playername, event.getPlayer());
							break;
						case NONE:
							break;
						case PRIVATE:
							reinforceSingle(event.getClickedBlock(), playername, event.getPlayer());
							break;
						case PUBLIC:
							reinforcePublic(event.getClickedBlock(), playername, event.getPlayer());
							break;
						default:
							break;
						}
						
						ChatHandler.info(event.getPlayer(), block.getX()+"/"+block.getY()+"/"+block.getZ()+" reinforced.");
					}
				}
			}
		}
	}

	private void reinforceGroup(Block block, String playername, Player player) {
		Material mat = fortyfy.getPlayerStateHolder().getMaterial(playername);
		int strength = fortyfy.getReinforcements().getStrength(mat);
		String groupname = fortyfy.getPlayerStateHolder()
				.getGroupNameForPlayer(playername);
		if (removeMaterialFromPlayer(player, mat, block.getType())) {
			try {
				fortyfy.getReinforcementDAO().reinforce(block, groupname, mat,
						fortyfy.getReinforcements().getCost(mat), strength,
						GROUP);
				logger.info("Reinforced " + block);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "REINFORCEMENT ERROR.", e);
				returnMaterial(player, mat, block.getType());
			}
		}
	}

	private void reinforceSingle(Block block, String playername, Player player) {
		Material mat = fortyfy.getPlayerStateHolder().getMaterial(playername);
		if (removeMaterialFromPlayer(player, mat, block.getType())) {
			try {
				int strength = fortyfy.getReinforcements().getStrength(mat);
				fortyfy.getReinforcementDAO().reinforce(block,
						player.getName(), mat,
						fortyfy.getReinforcements().getCost(mat), strength,
						PRIVATE);

			} catch (Exception e) {
				logger.log(Level.SEVERE, "REINFORCEMENT ERROR.", e);
				returnMaterial(player, mat, block.getType());
			}
		}
	}

	private void reinforcePublic(Block block, String playername, Player player) {
		Material mat = fortyfy.getPlayerStateHolder().getMaterial(playername);
		if (removeMaterialFromPlayer(player, mat, block.getType())) {
			try {
				int strength = fortyfy.getReinforcements().getStrength(mat);
				fortyfy.getReinforcementDAO().reinforce(block,
						player.getName(), mat,
						fortyfy.getReinforcements().getCost(mat), strength,
						PUBLIC);

			} catch (Exception e) {
				logger.log(Level.SEVERE, "REINFORCEMENT ERROR.", e);
				returnMaterial(player, mat, block.getType());
			}
		}
	}
}
