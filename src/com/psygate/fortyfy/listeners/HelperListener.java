package com.psygate.fortyfy.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class HelperListener extends AListener {
	public HelperListener(Fortyfy fy) {
		super(fy);
	}
	
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void entityExplode(EntityDamageByEntityEvent event) throws Exception {
		boolean lightning = (event.getEntity() instanceof LightningStrike || event.getDamager() instanceof LightningStrike);
		if(event.getEntity() instanceof Player) {
			if(!lightning) event.getEntity().getWorld().strikeLightning(event.getDamager().getLocation());
			event.setCancelled(true);
		} else if(event.getDamager() instanceof Player) {
			if(!lightning) event.getDamager().getWorld().strikeLightning(event.getEntity().getLocation());
			event.setDamage(1000);
			event.setCancelled(false);
		}
		
		if(event.getEntity() instanceof Player) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void entityFall(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void entityPoint(PlayerInteractEvent e) throws Exception {
		Block block = e.getClickedBlock();
		if(block == null) return;
		if(fortyfy.getReinforcementDAO().isReinforced(block)) {
			Vector3<Material, Integer, Integer> vec = fortyfy.getReinforcementDAO().getReinforcedWith(block);
			float broken = vec.getB();
			float breaks = vec.getC();
			int strength = (int) ((broken / breaks) * 100);
			ReinforcementMode mode = fortyfy.getReinforcementDAO().getReinforcementMode(block);
			ChatHandler.info(e.getPlayer(), "Block is reinforced @"+strength+"% ("+mode+")");
		}
		
		ChatHandler.info(e.getPlayer(), "Block: "+block.getTypeId()+" Data: "+block.getData());
	}
}
