package com.psygate.fortyfy.listeners;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class BedEnterListener extends AListener {

	public BedEnterListener(Fortyfy fortyfy) {
		super(fortyfy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void bedEnter(PlayerInteractEvent event) throws Exception {
		if(event.getClickedBlock() == null) return;
		if(beds.contains(event.getClickedBlock().getType())) {
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block bed = delegate(event.getClickedBlock());
				if(fortyfy.getReinforcementDAO().isReinforced(bed)) {
					ReinforcementMode mode = fortyfy.getReinforcementDAO().getReinforcementMode(bed);
					if(mode.equals(ReinforcementMode.PRIVATE)) {
						String reinfor = fortyfy.getReinforcementDAO().getReinforcedFor(bed);
						if(!event.getPlayer().getName().equals(reinfor)) {
							ChatHandler.error(event.getPlayer(), "You cannot sleep here. This bed is taken.");
							event.setCancelled(true);
						}
					} else if(mode.equals(ReinforcementMode.GROUP)) {
						String reinfor = fortyfy.getReinforcementDAO().getReinforcedFor(bed);
						if(!fortyfy.getGroupDAO().isMemberOf(reinfor, event.getPlayer().getName())) {
							ChatHandler.error(event.getPlayer(), "You cannot sleep here. This bed is taken.");
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}
	

}
