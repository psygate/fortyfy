package com.psygate.fortyfy.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.print.attribute.HashAttributeSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Bed;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.helpers.chat.ChatHandler;
import com.psygate.fortyfy.helpers.chat.Color;

import static org.bukkit.block.BlockFace.*;

public abstract class AListener implements Listener {
	protected Logger logger = Logger.getLogger(Fortyfy.LOGGERNAME);
	protected Fortyfy fortyfy;
	protected static final ArrayList<Integer> doors = new ArrayList<Integer>();
	protected static final ArrayList<BlockFace> chests = new ArrayList<BlockFace>();
	protected static final ArrayList<Material> beds = new ArrayList<Material>();
	protected static final BlockFace[] sixdir = new BlockFace[] { UP, DOWN,
			NORTH, EAST, WEST, SOUTH };
	protected static final ArrayList<Material> redstonepassthrough = new ArrayList<Material>();
	protected static final ArrayList<Material> liquidbreakable = new ArrayList<Material>();
	protected static final HashMap<Byte, BlockFace> beddirections = new HashMap<Byte, BlockFace>();
	protected int redstonedistance;

	static {
		doors.add(Material.WOOD_DOOR.getId());
		doors.add(Material.IRON_DOOR.getId());
		doors.add(Material.IRON_DOOR_BLOCK.getId());
		doors.add(Material.WOODEN_DOOR.getId());
		doors.add(Material.TRAP_DOOR.getId());
		chests.add(BlockFace.NORTH);
		chests.add(BlockFace.SOUTH);
		chests.add(BlockFace.EAST);
		chests.add(BlockFace.WEST);
		beds.add(Material.BED);
		beds.add(Material.BED_BLOCK);
		redstonepassthrough.add(Material.REDSTONE_WIRE);
		liquidbreakable.add(Material.REDSTONE_WIRE);
		liquidbreakable.add(Material.REDSTONE_TORCH_ON);
		liquidbreakable.add(Material.REDSTONE_TORCH_OFF);
		liquidbreakable.add(Material.POWERED_RAIL);
		liquidbreakable.add(Material.RAILS);
		beddirections.put((byte) 8, BlockFace.NORTH);
		beddirections.put((byte) 9, BlockFace.EAST);
		beddirections.put((byte) 10, BlockFace.SOUTH);
		beddirections.put((byte) 11, BlockFace.WEST);
	}

	public AListener(Fortyfy fortyfy) {
		this.fortyfy = fortyfy;
		this.redstonedistance = fortyfy.getRedstoneDistance();
	}

	protected void returnMaterial(Player player, Material mat, Material placed) {
		int base = fortyfy.getReinforcements().getCost(mat);
		int cost = (placed.equals(mat)) ? base + 1 : base;

		ChatHandler
				.error(player,
						"Reinforcement failed. Returning material and terminating reinforcement mode.");
		fortyfy.getPlayerStateHolder().remove(player.getName());
		player.getInventory().addItem(new ItemStack(mat, cost));
	}

	protected boolean removeMaterialFromPlayer(Player player, Material mat,
			Material placed) {
		int base = fortyfy.getReinforcements().getCost(mat);
		int cost = (placed.equals(mat)) ? base + 1 : base;
		if (!player.getInventory().containsAtLeast(new ItemStack(mat), cost)) {
			ChatHandler.notify(player, "Reinforcement material " + mat
					+ " depleted. Leaving fortification mode.");
			fortyfy.getPlayerStateHolder().remove(player.getName());
		} else {
			player.getInventory().removeItem(new ItemStack(mat, cost));

			return true;
		}

		return false;
	}

	protected Block delegate(Block block) throws Exception {
		Block oldblock = block;
		if (doors.contains(block.getTypeId())) {
			block = delegateDoor(block);
		} else if (block.getTypeId() == Material.CHEST.getId()) {
			boolean reinf = fortyfy.getReinforcementDAO().isReinforced(block);
			if (!reinf)
				block = delegateChest(block);
		} else if (block.getTypeId() == Material.BED.getId()
				|| block.getTypeId() == Material.BED_BLOCK.getId()) {
			block = delegateBed(block);
		}
		return block;
	}

	private Block delegateBed(Block block) {
		if ((block.getData() & 8) != 0) {
			return block.getRelative(beddirections.get(block.getData()));
		}

		return block;
	}

	private Block delegateDoor(Block block) {
		if ((block.getData() & 0x8) != 0)
			return block.getRelative(BlockFace.DOWN);
		else
			return block;
	}

	private Block delegateChest(Block block) throws Exception {
		if (!fortyfy.getReinforcementDAO().isReinforced(block)) {
			for (BlockFace face : chests) {
				Block newblock = block.getRelative(face);
				logger.info(newblock.getType().name());
				if (Material.CHEST.getId() == newblock.getTypeId())
					return newblock;
			}
		}

		return block;
	}

	protected boolean isPlayerClose(BlockEvent event) throws Exception {
		String rfor = fortyfy.getReinforcementDAO().getReinforcedFor(
				event.getBlock());
		ReinforcementMode mode = fortyfy.getReinforcementDAO()
				.getReinforcementMode(event.getBlock());
		if (mode.equals(ReinforcementMode.PRIVATE)) {
			return singleIsPlayerClose(rfor, event);
		} else if (mode.equals(ReinforcementMode.GROUP)) {
			Collection<String> members = fortyfy.getGroupDAO().getMembers(rfor);
			for (String mname : members) {
				if (singleIsPlayerClose(mname, event))
					return true;
			}
		} else if (mode.equals(ReinforcementMode.PUBLIC)) {
			return true;
		}

		return false;
	}

	private boolean singleIsPlayerClose(String name, BlockEvent event) {
		Player player = fortyfy.getServer().getPlayer(name);
		Block block = event.getBlock();
		if (player.isOnline()) {
			Location loc = player.getLocation();
			int x = loc.getBlockX() - block.getX();
			int y = loc.getBlockY() - block.getY();
			int z = loc.getBlockZ() - block.getZ();
			int dist = x * x + y * y + z * z;
			logger.info("Distance of " + name + " = " + dist);
			if (dist > redstonedistance) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
