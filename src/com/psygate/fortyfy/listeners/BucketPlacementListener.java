package com.psygate.fortyfy.listeners;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

import com.psygate.fortyfy.Fortyfy;

public class BucketPlacementListener extends AListener {

	public BucketPlacementListener(Fortyfy fortyfy) {
		super(fortyfy);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void bucketPlacement(PlayerBucketEmptyEvent event) throws Exception{
		Block block = event.getBlockClicked().getRelative(BlockFace.UP);
		
		if(fortyfy.getReinforcementDAO().isReinforced(block)) {
			event.setCancelled(true);
		}
	}
}
