package com.psygate.fortyfy.database.daos;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;

public interface IReinforcementDAO {
	public void reinforce(Block block, String reinforceFor,
			Material reinforcement, int count, int strength,
			ReinforcementMode mode) throws Exception;

	public boolean isReinforced(Block block) throws Exception;

	public int getReinforcementStrength(Block block) throws Exception;

	public void updateReinforcementStrength(Block block, int i)
			throws Exception;

	public String getReinforcedFor(Block block) throws Exception;

	public ReinforcementMode getReinforcementMode(Block block) throws Exception;

	public boolean mayBypass(Block block, String name) throws Exception;

	/**
	 * Returns a vector containing the reinforcement material, the remaining
	 * strength of the material, and the full strength, at which it was
	 * reinforced.
	 * 
	 * @param block
	 *            The block to query for reinforcement data.
	 * @return Vector3<Material, Integer, Integer> ->
	 *         Vector3<ReinforcementMaterial, Remaining strength of material,
	 *         Full strength of material at creation>
	 * @throws Exception
	 */
	public Vector3<Material, Integer, Integer> getReinforcedWith(Block block)
			throws Exception;
	
	/**
	 * Returns the count of blocks of reinforcement material, that were consumed in the reinforcement process.
	 * @param block Block to check for.
	 * @return Count of reinforcement material, consumed in the process of reinforcing the block.
	 * @throws Exception
	 */
	public int getReinforcementCount(Block block) throws Exception;
	public boolean removeReinforced(Block block) throws Exception;

	public int performSanityCheck(CommandSender sender, Fortyfy fortyfy)
			throws Exception;
	
	/**
	 * General method to be called on termination of the plugin.
	 */
	public void shutdown();

}
