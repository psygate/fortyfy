package com.psygate.fortyfy.database.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;

public class SimpleReinforcementDAO extends ADao implements IReinforcementDAO {
	// private HashMap<String, String> sqlstatements = new HashMap<String,
	// String>();
	private Fortyfy fy;

	public SimpleReinforcementDAO(Database db, Fortyfy fy) throws SQLException {
		super("ReinforcementDAO.");
		this.db = db;
		this.fy = fy;
	}

	@Override
	public void reinforce(Block block, String reinforceFor,
			Material reinforcement, int count, int strength,
			ReinforcementMode mode) throws Exception {
		PreparedStatement stat = prepare("reinforce");
		// CREATE TABLE IF NOT EXISTS $prefix$reinforcements(x INTEGER, y
		// INTEGER, z INTEGER, world VARCHAR(64),
		// reinforcer VARCHAR(32), material INTEGER, totalbreaks INTEGER,
		// remainingstrength INTEGER,
		// mode INTEGER, materialcount INTEGER, PRIMARY KEY(x,y,z));");

		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		stat.setString(5, reinforceFor);
		stat.setInt(6, reinforcement.getId());
		stat.setInt(7, strength);
		stat.setInt(8, strength);
		stat.setInt(9, mode.valueOf());
		stat.setInt(10, count);
		stat.execute();
		logger.info(block.getX() + "/" + block.getY() + "/" + block.getZ()
				+ "@" + block.getWorld().getName() + " reinforced for " + reinforceFor
				+ " with " + count + "x" + reinforcement.name() + " @"
				+ strength + " Material Strength: " + strength);
	}

	public boolean isReinforced(Block block) throws SQLException {
		PreparedStatement stat = prepare("isreinforced");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		if (rs.next()) {
			rs.close();
			return true;
		} else {
			rs.close();
			return false;
		}
	}

	public int getReinforcementStrength(Block block) throws SQLException {
		PreparedStatement stat = prepare("isreinforced");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		int strength = 0;
		if (rs.next()) {
			strength = rs.getInt(1);
			rs.close();
			logger.info("Strength requested: " + strength);
			return strength;
		} else {
			rs.close();
			return strength;
		}
	}

	@Override
	public void updateReinforcementStrength(Block block, int i)
			throws Exception {
		logger.info("Updating strength with " + i);
		if (i > 1) {
			PreparedStatement stat = prepare("updatestrength");
			stat.setInt(1, i);
			stat.setInt(2, block.getX());
			stat.setInt(3, block.getY());
			stat.setInt(4, block.getZ());
			stat.setString(5, block.getWorld().getName());
			stat.execute();
//			if (stat.executeUpdate() <= 0) {
//				logger.severe("Block not found.");
//				logger.severe(block.getX() + "/" + block.getY() + "/"
//						+ block.getZ() + "@" + block.getWorld().getName());
//			}
		} else {
			PreparedStatement stat = prepare("removereinforcement");
			stat.setInt(1, block.getX());
			stat.setInt(2, block.getY());
			stat.setInt(3, block.getZ());
			stat.setString(4, block.getWorld().getName());
			stat.execute();
		}
		logger.info(block.getX() + "/" + block.getY() + "/" + block.getZ()
				+ " @" + i);
	}

	@Override
	public String getReinforcedFor(Block block) throws Exception {
		PreparedStatement stat = prepare("reinforcedfor");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		String out = null;
		if (rs.next()) {
			out = rs.getString(1);
		}

		rs.close();

		return out;
	}

	@Override
	public ReinforcementMode getReinforcementMode(Block block) throws Exception {
		PreparedStatement stat = prepare("getreinforcementmode");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		ReinforcementMode mode = ReinforcementMode.NONE;
		if (rs.next()) {
			mode = ReinforcementMode.valueOf(rs.getInt(1));
		}

		rs.close();

		return mode;
	}

	@Override
	public boolean mayBypass(Block block, String name) throws Exception {
		ReinforcementMode mode = getReinforcementMode(block);
		if (mode.equals(ReinforcementMode.NONE))
			return true;
		String reinfor = getReinforcedFor(block);
		if ((mode.equals(ReinforcementMode.PRIVATE) || mode
				.equals(ReinforcementMode.PUBLIC)) && reinfor.equals(name)) {
			return true;
		} else if (mode.equals(ReinforcementMode.GROUP)
				&& fy.getGroupDAO().isMemberOf(reinfor, name)) {
			return true;
		}

		return false;
	}

	@Override
	public Vector3<Material, Integer, Integer> getReinforcedWith(Block block)
			throws Exception {
		PreparedStatement stat = prepare("reinforcedwith");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		Vector3<Material, Integer, Integer> ret = new Vector3<Material, Integer, Integer>(
				Material.FEATHER, 1, 1);
		if (rs.next()) {
			ret.setA(Material.getMaterial(rs.getInt(1)));
			ret.setB(rs.getInt(2));
			ret.setC(rs.getInt(3));
			logger.info("Was reinforced with " + rs.getInt(1) + ". "
					+ (Material.getMaterial(rs.getInt(1)).name()));
		}
		rs.close();

		return ret;
	}

	@Override
	public boolean removeReinforced(Block block) throws Exception {
		PreparedStatement stat = prepare("removereinforcement");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		return stat.execute();
	}

	@Override
	public int performSanityCheck(CommandSender sender, Fortyfy fortyfy)
			throws Exception {
		PreparedStatement stat = prepare("getallreinforced");
		ResultSet rs = stat.executeQuery();
		PreparedStatement drop = prepare("removereinforcement");
		int cleancounter = 0;
		while (rs.next()) {
			String name = rs.getString(4);
			int x = rs.getInt(1);
			int y = rs.getInt(2);
			int z = rs.getInt(3);
			Block now = fortyfy.getServer().getWorld(name).getBlockAt(x, y, z);
			if (Material.AIR.equals(now.getType())) {
				cleancounter++;
				drop.setInt(1, x);
				drop.setInt(2, y);
				drop.setInt(3, z);
				drop.setString(4, name);
				drop.addBatch();
			}
		}
		drop.executeBatch();
		return cleancounter;
	}

	@Override
	public int getReinforcementCount(Block block) throws Exception {
		PreparedStatement stat = prepare("reinforcementcount");
		stat.setInt(1, block.getX());
		stat.setInt(2, block.getY());
		stat.setInt(3, block.getZ());
		stat.setString(4, block.getWorld().getName());
		ResultSet rs = stat.executeQuery();
		int ret = 0;
		if (rs.next()) {
			ret = rs.getInt(1);
		}

		return ret;
	}
	
	@Override
	public void shutdown() {
		//Does nothing in this dao.
	}
}
