package com.psygate.fortyfy.database.daos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.helpers.BundleParser;

public abstract class ADao {
	protected Database db;
	protected Logger logger = Logger.getLogger(Fortyfy.LOGGERNAME);
	protected static final BundleParser parser = new BundleParser();
	private HashMap<String, String> sqlstatements = new HashMap<String, String>();

	public ADao(String prefix) {
		addtToBundle(prefix);
	}

	protected void addtToBundle(String prefix) {
		for (String key : parser.getKeys()) {
			if (key.startsWith(prefix)) {
				String mkey = key.replace(prefix, "");
				sqlstatements.put(mkey, parser.getString(key));
			} else if (key.startsWith(prefix)) {
				String mkey = key.replace(prefix, "");
				sqlstatements.put(mkey, parser.getString(key));
			}
		}
	}
	
	protected PreparedStatement prepare(String name) throws SQLException {
		return db.prepareStatement(sqlstatements.get(name));
	}
}
