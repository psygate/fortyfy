package com.psygate.fortyfy.database.daos;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.ReinforcementMode;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;

public class CachingReinforcementDAO implements IReinforcementDAO {
	private HashMap<Integer, CacheBlock> cachemap;
	
	public CachingReinforcementDAO(int cachesize) {
		cachemap = new HashMap<Integer, CachingReinforcementDAO.CacheBlock>(cachesize);
	}
	
	private class CacheBlock {
		private int x;
		private int y;
		private int z;
		private int breaks;
		private int strength;
		private String reinfor;
		private Material reinforcement;
		private int hash = 0;
		private boolean hashed = false;
		
		public CacheBlock(int x, int y, int z, int breaks, int strength,
				String reinfor, Material reinforcement) {
			super();
			this.x = x;
			this.y = y;
			this.z = z;
			this.breaks = breaks;
			this.strength = strength;
			this.reinfor = reinfor;
			this.reinforcement = reinforcement;
		}
		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getY() {
			return y;
		}
		public void setY(int y) {
			this.y = y;
		}
		public int getZ() {
			return z;
		}
		public void setZ(int z) {
			this.z = z;
		}
		public int getBreaks() {
			return breaks;
		}
		public void setBreaks(int breaks) {
			this.breaks = breaks;
		}
		public int getStrength() {
			return strength;
		}
		public void setStrength(int strength) {
			this.strength = strength;
		}
		public String getReinfor() {
			return reinfor;
		}
		public void setReinfor(String reinfor) {
			this.reinfor = reinfor;
		}
		public Material getReinforcement() {
			return reinforcement;
		}
		public void setReinforcement(Material reinforcement) {
			this.reinforcement = reinforcement;
		}
		
		@Override
		public int hashCode() {
			if(!hashed) {
				hash = x & 0xffff0000;
				hash += y & 0xffff;
				hash ^= z;
				hashed = true;
			}
			
			return hash;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj.hashCode() != this.hashCode()) return false;
			if(!(obj instanceof CacheBlock)) return false;
			
			CacheBlock other = (CacheBlock)obj;
			return other.x == this.x && other.y == this.y && other.z == this.z;
		}
		
	}
	
	@Override
	public void reinforce(Block block, String reinforceFor,
			Material reinforcement, int count, int strength,
			ReinforcementMode mode) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isReinforced(Block block) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getReinforcementStrength(Block block) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateReinforcementStrength(Block block, int i)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String getReinforcedFor(Block block) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReinforcementMode getReinforcementMode(Block block) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean mayBypass(Block block, String name) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Vector3<Material, Integer, Integer> getReinforcedWith(Block block)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getReinforcementCount(Block block) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean removeReinforced(Block block) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int performSanityCheck(CommandSender sender, Fortyfy fortyfy)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

}
