package com.psygate.fortyfy.database.daos;

import java.util.Collection;

public interface IGroupDAO {
	public void createGroup(String creator, String groupname)
			throws Exception;

	public void createGroupWithPassword(String creator, String groupname,
			String password) throws Exception;

	public boolean isGroupExistent(String name) throws Exception;

	public boolean isCreator(String name, String groupname) throws Exception;

	public void deleteGroup(String string) throws Exception;

	public void moveGroup(String groupname, String name) throws Exception;

	public void renameGroup(String oldname, String newname) throws Exception;

	public void addModerator(String name, String groupname) throws Exception;

	public boolean isPasswordProtected(String groupname) throws Exception;

	public String getPassword(String groupname) throws Exception;

	public void addMember(String groupname, String name) throws Exception;

	public void leaveGroup(String name, String groupname) throws Exception;

	public boolean isModerator(String groupname, String name) throws Exception;

	public void kickMember(String groupname, String name) throws Exception;

	public boolean isKicked(String groupname, String name) throws Exception;

	public void setPassword(String groupname, String password)
			throws Exception;

	public Collection<String> getGroups(String name) throws Exception;

	public Collection<String> getOwnedGroups(String name) throws Exception;

	public void removePassword(String string) throws Exception;

	public boolean isMemberOf(String reinfor, String name) throws Exception;

	public Collection<String> getMembers(String rfor) throws Exception;
	
	public int getGroupCount(String name) throws Exception;
	
	/**
	 * General method to be called on termination of the plugin.
	 */
	public void shutdown();
}
