package com.psygate.fortyfy.database.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import com.psygate.fortyfy.database.Database;

/*
 * Add an iscreator feature
 */
public class SimpleGroupDAO extends ADao implements IGroupDAO {
	public final static int NOPASSWORD = 0;
	public final static int PASSWORD = 1;
	public final static int NOMOD = 0;
	public final static int MOD = 1;
	public final static int KICKED = 1;
	public final static int NOT_KICKED = 0;

	public SimpleGroupDAO(Database db) throws SQLException {
		super("GroupDAO.");
		this.db = db;
	}

	@Override
	public void createGroup(String creator, String groupname)
			throws Exception {
		PreparedStatement stat = prepare("creategroup");
		stat.setString(1, groupname);
		stat.setString(2, creator);
		stat.setInt(3, NOPASSWORD);
		logger.info("Creating: "+groupname+" Creator: "+creator);
		stat.execute();
		logger.info("Created: "+groupname+" Creator: "+creator);
		addModerator(creator, groupname);
	}

	@Override
	public void createGroupWithPassword(String creator, String groupname,
			String password) throws Exception {
		PreparedStatement stat = prepare("creategrouppw");
		stat.setString(1, groupname);
		stat.setString(2, creator);
		stat.execute();
		PreparedStatement pwstat = prepare("insertpw");
		pwstat.setInt(3, PASSWORD);
		pwstat.setString(4, groupname);
		pwstat.setString(5, password);
		pwstat.execute();
		addModerator(creator, groupname);
	}

	@Override
	public boolean isGroupExistent(String name) throws Exception {
		PreparedStatement stat = prepare("getgroup");
		stat.setString(1, name);
		ResultSet rs = stat.executeQuery();
		boolean out = rs.next();
		rs.close();
		return out;
	}

	@Override
	public boolean isCreator(String name, String groupname) throws Exception {
		PreparedStatement stat = prepare("getgroupcreator");
		stat.setString(1, groupname);
		ResultSet rs = stat.executeQuery();
		boolean out = false;
		if (!rs.next()) {
			rs.close();
		} else {
			out = rs.getString(1).equals(name);
			logger.info("Leader of "+groupname+" is "+rs.getString(1)+" requested: "+name+" -- "+rs.getString(1).equals(name));
			rs.close();
		}
		return out;
	}

	@Override
	public void deleteGroup(String string) throws Exception {
		PreparedStatement stat = prepare("dropgroup");
		stat.setString(1, string);
		stat.execute();
	}

	@Override
	public void moveGroup(String groupname, String name) throws Exception {
		PreparedStatement stat = prepare("delegategroup");
		stat.setString(1, name);
		stat.setString(2, groupname);
		stat.execute();
	}

	@Override
	public void renameGroup(String oldname, String newname) throws Exception {
		PreparedStatement stat = prepare("renamegroup");
		stat.setString(1, newname);
		stat.setString(2, oldname);
		stat.execute();
	}

	@Override
	public void addModerator(String name, String groupname) throws Exception {
		PreparedStatement stat = prepare("addmember");
		logger.info("Adding moderator "+name+" to "+groupname);
		stat.setString(1, name);
		stat.setInt(2, MOD);
		stat.setString(3, groupname);
		stat.setInt(4, NOT_KICKED);
		stat.execute();
	}

	@Override
	public boolean isPasswordProtected(String groupname) throws Exception {
		PreparedStatement stat = prepare("ispasswordprotected");
		stat.setString(1, groupname);
		ResultSet rs = stat.executeQuery();
		boolean ispwp = false;
		if (rs.next()) {
			if(rs.getInt("pwp") == PASSWORD) ispwp = true;
		}
		rs.close();
		
		return ispwp;
	}

	@Override
	public String getPassword(String groupname) throws Exception {
		PreparedStatement stat = prepare("getpassword");
		stat.setString(1, groupname);
		ResultSet rs = stat.executeQuery();
		if(!rs.next()) return null;
		String pw = rs.getString(1);
		rs.close();
		return pw;
	}

	@Override
	public void addMember(String groupname, String name) throws Exception {
		PreparedStatement stat = prepare("addmember");
		stat.setString(1, name);
		stat.setInt(2, NOMOD);
		stat.setString(3, groupname);
		stat.setInt(4, NOT_KICKED);
		stat.execute();
	}

	@Override
	public void leaveGroup(String name, String groupname) throws Exception {
		PreparedStatement stat = prepare("removemember");
		stat.setString(1, name);
		stat.setString(2, groupname);
		stat.execute();
	}

	@Override
	public boolean isModerator(String groupname, String name) throws Exception {
		PreparedStatement stat = prepare("ismoderator");
		stat.setString(1, groupname);
		stat.setString(2, name);
		ResultSet rs = stat.executeQuery();
		boolean ismod = false;
		if(rs.next()) {
			ismod = (rs.getInt(1) == MOD) && (rs.getInt(2) == NOT_KICKED);
		}
		rs.close();
		return ismod;
	}

	@Override
	public void kickMember(String groupname, String name) throws Exception {
		//UPDATE $prefix$members SET kicked=?, ismod=? WHERE groupname=? AND name=?;
		PreparedStatement stat = prepare("kickmember");
		stat.setInt(1, KICKED);
		stat.setInt(2, NOMOD);
		stat.setString(3, groupname);
		stat.setString(4, name);
		stat.execute();
	}

	@Override
	public boolean isKicked(String groupname, String name) throws Exception {
		PreparedStatement stat = prepare("ismoderator");
		stat.setString(1, groupname);
		stat.setString(2, name);
		ResultSet rs = stat.executeQuery();
		boolean iskicked = false;
		if(rs.next()) {
			iskicked = (rs.getInt(2) == NOT_KICKED);
		}
		rs.close();
		return iskicked;
	}

	@Override
	public void setPassword(String groupname, String password)
			throws Exception {
		PreparedStatement stat = prepare("updatepassword");
		stat.setString(1, password);
		stat.setString(2, groupname);
		stat.execute();
		stat=prepare("updatepwp");
		stat.setInt(1, PASSWORD);
		stat.setString(2, groupname);
		stat.execute();
	}

	@Override
	public Collection<String> getGroups(String name) throws Exception {
		LinkedList<String> groups = new LinkedList<String>();
		PreparedStatement stat = prepare("getgroupsofuser");
		stat.setString(1, name);
		ResultSet rs = stat.executeQuery();
		while(rs.next()) {
			groups.add(rs.getString(1));
		}
		rs.close();
		
		return groups;
	}

	@Override
	public Collection<String> getOwnedGroups(String name) throws Exception {
		LinkedList<String> groups = new LinkedList<String>();
		PreparedStatement stat = prepare("getownedgroups");
		stat.setString(1, name);
		ResultSet rs = stat.executeQuery();
		while(rs.next()) {
			groups.add(rs.getString(1));
		}
		rs.close();
		
		return groups;
	}

	@Override
	public void removePassword(String string) throws SQLException {
		PreparedStatement stat = prepare("removepassword");
		stat.setString(1, string);
		stat.execute();
		stat = prepare("updatepwp");
		stat.setInt(1, NOPASSWORD);
		stat.setString(2, string);
		stat.execute();
	}

	@Override
	public boolean isMemberOf(String reinfor, String name) throws Exception {
		PreparedStatement stat = prepare("ismemberof");
		//SELECT name FROM $prefix$members WHERE groupname=? AND name=? AND kicked=?;
		logger.info("Requesting member "+name+" of "+reinfor);
		stat.setString(1, reinfor);
		stat.setString(2, name);
		stat.setInt(3, NOT_KICKED);
		ResultSet rs = stat.executeQuery();
		boolean is = false;
		if(rs.next()) {
			is = true;
		}
		
		logger.info(name+" is member of "+reinfor+": "+is);
		logger.info(getMembers(reinfor).toString());
		rs.close();
		return is;
	}

	@Override
	public Collection<String> getMembers(String rfor) throws Exception {
		PreparedStatement stat = prepare("getmembers");
		stat.setString(1, rfor);
		stat.setInt(2, NOT_KICKED);
		ResultSet set = stat.executeQuery();
		LinkedList<String> names = new LinkedList<String>();
		while(set.next()) {
			names.add(set.getString(1));
		}
		set.close();
		return names;
	}

	@Override
	public int getGroupCount(String name) throws Exception {
		PreparedStatement stat = prepare("getgroupcount");
		stat.setString(1, name);
		stat.setInt(2, NOT_KICKED);
		ResultSet rs = stat.executeQuery();
		int count = 0;
		if(rs.next()) {
			count = rs.getInt(1);
		}
		
		rs.close();
		return count;
	}

	@Override
	public void shutdown() {
		//Does nothing in this dao.
	}
}
