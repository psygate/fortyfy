package com.psygate.fortyfy.database.exceptions;

public class UnsupportedDBException extends DatabaseException {
	private static final long serialVersionUID = 1814354905918727897L;

	public UnsupportedDBException(String message) {
		super(message);
	}
}
