package com.psygate.fortyfy.database.exceptions;

public class UnsupportedDBPortException extends DatabaseException {
	private static final long serialVersionUID = 1L;

	public UnsupportedDBPortException(String message) {
		super(message);
	}
}
