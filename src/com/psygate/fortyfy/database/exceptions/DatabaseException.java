package com.psygate.fortyfy.database.exceptions;

public abstract class DatabaseException extends Exception {
	private static final long serialVersionUID = 1L;
	public DatabaseException(String message) {
		super(message);
	}
}
