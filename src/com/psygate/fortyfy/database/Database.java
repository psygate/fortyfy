package com.psygate.fortyfy.database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import org.hsqldb.Server;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.database.exceptions.UnsupportedDBException;
import com.psygate.fortyfy.database.exceptions.UnsupportedDBPortException;
import com.psygate.fortyfy.helpers.BundleParser;

public class Database {
	private DatabaseType type = null;
	private String user = null;
	private String password = null;
	private String prefix = "";
	private String driver;
	private String connectString;
	private Connection con = null;
	private boolean reset = false;
	private static final BundleParser parser = new BundleParser();
	private Server server = null;

	public Database(File file, String prefix) throws UnsupportedDBException,
			UnsupportedDBPortException {
		super();

		this.type = DatabaseType.SQLITE;
		this.prefix = prefix;
		this.driver = "org.sqlite.JDBC";
		this.connectString = "jdbc:sqlite:" + file.getPath();
	}

	public Database(DatabaseType type, String user, String password,
			String url, String prefix, Fortyfy fy) {
		if (type.equals(DatabaseType.POSTGRES)) {
			this.type = DatabaseType.POSTGRES;
			this.connectString = url;
			this.user = user;
			this.password = password;
			this.prefix = prefix;
			this.driver = "org.postgresql.Driver";
		} else if (type.equals(DatabaseType.MYSQL)) {
			this.type = DatabaseType.MYSQL;
			this.connectString = url;
			this.user = user;
			this.password = password;
			this.prefix = prefix;
			this.driver = "org.mysql.Driver";
		} else if (type.equals(DatabaseType.HSQLDB)) {
			this.type = DatabaseType.HSQLDB;
			this.connectString = url;
			this.user = user;
			this.password = password;
			this.prefix = prefix;
			this.driver = "org.hsqldb.jdbcDriver";
			System.setProperty("hsqldb.reconfig_logging", "false");
			server = new Server();
			server.setDaemon(true);
			server.setDatabasePath(0,
					(new File(fy.getDataFolder(), "minecraft")).getPath());
			server.setDatabaseName(0, "minecraft");
			server.setSilent(true);
			server.setLogWriter(null);
			server.start();
		}
	}

	public void resetOnStartup() {
		this.reset = true;
	}

	public void bootStrap() throws ClassNotFoundException, SQLException,
			IOException {
		Class.forName(driver);

		if (type.equals(DatabaseType.SQLITE)) {
			con = DriverManager.getConnection(connectString);
			con.createStatement().execute("PRAGMA foreign_keys=ON;");
		} else {
			con = DriverManager.getConnection(connectString, user, password);
		}

		if (reset)
			dropDatabase();
		runCreate();
		con.setAutoCommit(true);
		con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
	}

	private void dropDatabase() throws IOException, SQLException {
		Statement stat = con.createStatement();
		boolean auto = con.getAutoCommit();
		con.setAutoCommit(false);
		for (String key : parser.getKeys()) {
			if (key.startsWith("DB.delete")) {
				stat.addBatch(parser.getString(key).replace("$prefix$", prefix));
			}
		}
		stat.executeBatch();
		con.commit();
		con.setAutoCommit(auto);
	}

	public void createDatabase() throws SQLException {
		runCreate();
	}

	public Statement getStatement() throws SQLException {
		return con.createStatement();
	}

	public void shutdown() throws SQLException {
		if (con != null) {
			if (!con.getAutoCommit())
				con.commit();
			con.close();
		}
		if (server != null) {
			server.shutdown();
		}
	}

	public PreparedStatement prepareStatement(String statement)
			throws SQLException {
		statement = statement.replace("$prefix$", prefix);
		return con.prepareStatement(statement);
	}

	private void runCreate() throws SQLException {
		boolean auto = con.getAutoCommit();
		con.setAutoCommit(false);
		Statement stat = con.createStatement();
		for (String key : parser.getKeys()) {
			String rkey = key.replaceAll("DB\\.[0-9+]\\.", "DB.create");
			if (rkey.startsWith("DB.create")) {
				try {
					stat.execute(parser.getString(key).replace("$prefix$",
							prefix));
				} catch (SQLException e) {
					throw new SQLException(parser.getString(key).replace(
							"$prefix$", prefix), e);
				}
			}
		}

		con.commit();
		con.setAutoCommit(auto);
	}

	public Connection getConnection() {
		return con;
	}

	public enum DatabaseType {
		POSTGRES, MYSQL, SQLITE, HSQLDB;
	}
}
