package com.psygate.fortyfy.fortification;

import java.util.HashMap;
import java.util.TreeSet;

import org.bukkit.Material;

import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector3;

public class Reinforcements {
	private HashMap<Material, Vector3<Integer, Integer, Material>> fortifications = new HashMap<Material, Vector3<Integer, Integer, Material>>();
	private TreeSet<Material> reinforceable = new TreeSet<Material>();
	private TreeSet<Material> nonreinforceable = new TreeSet<Material>();
	
	public Reinforcements() {
		for(Material mat : Material.values()) {
			reinforceable.add(mat);
		}
	}
	
	public boolean isFortification(Material m) {
		return fortifications.containsKey(m);
	}
	
	public void addFortification(Material m, int strength, int required, Material flasher) {
		if(m == null || flasher == null) return;
		fortifications.put(m, new Vector3<Integer, Integer, Material>(strength, required, flasher));
	}
	
	public void removeFortification(Material m) {
		fortifications.remove(m);
	}
	
	public boolean isReinforceable(Material m) {
		return reinforceable.contains(m);
	}
	
	public boolean isNonReinforceable(Material m) {
		return nonreinforceable.contains(m);
	}
	
	public void addNonReinforceable(Material mat) {
		if(mat == null) return;
		nonreinforceable.add(mat);
		reinforceable.remove(mat);
	}
	
	public Vector3<Integer, Integer, Material> getFortification(Material m) {
		return fortifications.get(m);
	}

	public int getCost(Material mat) {
		return fortifications.get(mat).getB();
	}

	public int getStrength(Material mat) {
		return fortifications.get(mat).getA();
	}
}
