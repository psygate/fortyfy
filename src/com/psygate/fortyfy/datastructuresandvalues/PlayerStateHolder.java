package com.psygate.fortyfy.datastructuresandvalues;

import java.util.HashMap;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.TreeSet;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.psygate.fortyfy.Fortyfy;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector4;
import com.psygate.fortyfy.helpers.chat.ChatHandler;

public class PlayerStateHolder {
	private HashMap<String, Vector4<Material, ReinforcementMode, Long, String>> playerdata = new HashMap<String, Vector4<Material, ReinforcementMode, Long, String>>();
	private Fortyfy fortyfy;
	private static int KICK_TIME = 30 * 1000;
	private SortedSet<String> bypassing = new TreeSet<String>();
	private SortedMap<String, Long> infomode = new TreeMap<String, Long>();
	
	public PlayerStateHolder(Fortyfy fortyfy) {
		this.fortyfy = fortyfy;
		fortyfy.schedule(new Vacuum(), (KICK_TIME / 1000) * 20);
	}

	/**
	 * Reinforces for a single player on placement of a block.
	 * 
	 * @param player
	 * @param mat
	 * @param mode
	 */
	public void setPlayerState(String player, Material mat,
			ReinforcementMode mode) {
		synchronized (playerdata) {
			if (playerdata.containsKey(player))
				playerdata.remove(player);
			playerdata.put(player,
					new Vector4<Material, ReinforcementMode, Long, String>(mat,
							mode, System.currentTimeMillis(), null));
		}
	}

	public Material getMaterial(String player) {
		synchronized (playerdata) {
			return playerdata.get(player).getA();
		}
	}

	public boolean isReinforcing(String player) {
		synchronized (playerdata) {
			return playerdata.get(player) != null;
		}
	}

	public void refreshMode(String player) {
		synchronized (playerdata) {
			playerdata.get(player).setC(System.currentTimeMillis());
		}
	}

	public boolean isReinforceingAndRefresh(String player) {
		synchronized (playerdata) {
			if (playerdata.get(player) != null) {
				refreshMode(player);
				return true;
			}
		}

		return false;
	}

	public ReinforcementMode getPlayerMode(String player) {
		synchronized (playerdata) {
			return playerdata.get(player).getB();
		}
	}

	public void remove(String name) {
		synchronized (playerdata) {
			playerdata.remove(name);
		}
	}

	public String getGroupNameForPlayer(String name) {
		synchronized (playerdata) {
			return playerdata.get(name).getD();
		}
	}

	public void terminate() {

	}

	/**
	 * Reinforces for a group on placement of a block.
	 * 
	 * @param name
	 * @param type
	 * @param group
	 * @param string
	 */
	public void setPlayerState(String name, Material type,
			ReinforcementMode group, String string) {
		synchronized (playerdata) {
			playerdata.put(name,
					new Vector4<Material, ReinforcementMode, Long, String>(
							type, group, System.currentTimeMillis(), string));
		}

	}

	public void setBypassing(String name) {
		this.bypassing.add(name);
	}

	private class Vacuum extends TimerTask {
		public Vacuum() {
			super();
		}

		@Override
		public void run() {
			long start = System.currentTimeMillis();
			synchronized (playerdata) {
				for (String key : playerdata.keySet()) {
					if (start - playerdata.get(key).getC() > KICK_TIME) {
						playerdata.remove(key);
						ChatHandler.notify(fortyfy.getServer().getPlayer(key),
								"Leaving fortification mode.");
					}
				}
			}

			synchronized (infomode) {
				for (String key : infomode.keySet()) {
					if (start - infomode.get(key) > KICK_TIME) {
						infomode.remove(key);
						ChatHandler.notify(fortyfy.getServer().getPlayer(key),
								"INFO mode terminated.");
					}
				}
			}
		}
	}

	public boolean isBypassing(String name) {
		return bypassing.contains(name);
	}

	public void setStopBypassing(String name) {
		bypassing.remove(name);
	}

	public void setInformationMode(String name) {
		infomode.put(name, System.currentTimeMillis());
	}

	public boolean isInfoMode(String name) {
		return infomode.containsKey(name);
	}

	public boolean isClickReinforcing(String name) {
		if (!this.playerdata.containsKey(name))
			return false;
		return ReinforcementMode.isClickMode(this.playerdata.get(name).getB());
	}
}