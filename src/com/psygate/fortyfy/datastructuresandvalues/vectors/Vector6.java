package com.psygate.fortyfy.datastructuresandvalues.vectors;

public class Vector6<A, B, C, D, E, F> extends Vector5<A, B, C, D, E> {
	private F f;
	
	public Vector6(A a, B b, C c, D d, E e, F f) {
		super(a, b, c, d, e);
		// TODO Auto-generated constructor stub
	}

	public F getF() {
		return f;
	}

	public void setF(F f) {
		this.f = f;
	}
}
