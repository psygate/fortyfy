package com.psygate.fortyfy.datastructuresandvalues.vectors;

public class Vector3<A, B, C> extends Vector2<A, B> {

	protected C c;

	public Vector3(A a, B b, C c) {
		super(a, b);
		this.c = c;
	}

	public C getC() {
		return c;
	}

	public void setC(C c) {
		this.c = c;

	}
}
