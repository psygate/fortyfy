package com.psygate.fortyfy.datastructuresandvalues.vectors;

public class Vector5<A, B, C, D, E> extends Vector4<A, B, C, D> {
	private E e;
	
	public Vector5(A a, B b, C c, D d, E e) {
		super(a, b, c, d);
		this.e = e;
	}
	
	public E getE() {
		return e;
	}
	
	public void setE(E e) {
		this.e = e;
	}
}
