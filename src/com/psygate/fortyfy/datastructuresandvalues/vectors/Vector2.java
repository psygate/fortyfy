package com.psygate.fortyfy.datastructuresandvalues.vectors;

public class Vector2<A, B> {
	protected A a;
	protected B b;

	public Vector2(A a, B b) {
		this.a = a;
		this.b = b;
	}

	public A getA() {
		return a;
	}

	public void setA(A a) {
		this.a = a;
	}

	public B getB() {
		return b;
	}

	public void setB(B b) {
		this.b = b;
	}
	
	public String toString() {
		return "Vector2("+a+", "+b+")";
	}
}
