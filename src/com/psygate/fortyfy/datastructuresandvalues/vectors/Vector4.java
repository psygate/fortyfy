package com.psygate.fortyfy.datastructuresandvalues.vectors;

public class Vector4<A, B, C, D> extends Vector3<A, B, C> {

	protected D d;

	public Vector4(A a, B b, C c, D d) {
		super(a, b, c);
		this.d = d;
	}

	public D getD() {
		return d;
	}

	public void setD(D d) {
		this.d = d;

	}
}
