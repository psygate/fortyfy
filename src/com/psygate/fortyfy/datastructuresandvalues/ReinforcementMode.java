package com.psygate.fortyfy.datastructuresandvalues;

public enum ReinforcementMode {
	NONE(0), PRIVATE(1), PUBLIC(2), GROUP(3), CLICK_PRIVATE(4), CLICK_PUBLIC(5), CLICK_GROUP(
			6);

	private int value;

	private ReinforcementMode(int value) {
		this.value = value;
	}

	public int valueOf() {
		return value;
	}

	public static ReinforcementMode translate(ReinforcementMode mode) {
		switch (mode) {
		case NONE:
			return NONE;
		case CLICK_PRIVATE:
			return PRIVATE;
		case CLICK_PUBLIC:
			return PUBLIC;
		case CLICK_GROUP:
			return GROUP;
		case PRIVATE:
			return PRIVATE;
		case GROUP:
			return GROUP;
		case PUBLIC:
			return PUBLIC;
		default:
			return NONE;
		}
	}

	public static ReinforcementMode valueOf(int val) {
		switch (val) {
		case 0:
			return NONE;
		case 1:
			return PRIVATE;
		case 2:
			return PUBLIC;
		case 3:
			return GROUP;
		case 4:
			return CLICK_PRIVATE;
		case 5:
			return CLICK_PUBLIC;
		case 6:
			return CLICK_GROUP;
		default:
			return NONE;
		}
	}

	public static boolean isClickMode(ReinforcementMode current) {
		if (current.value >= 4)
			return true;
		return false;
	}
}
