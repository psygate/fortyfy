package com.psygate.fortyfy.exceptions;

public class NotImplementedYetException extends Error {
	private static final long serialVersionUID = 1L;
	
	public NotImplementedYetException(String message) {
		super(message);
	}
}
