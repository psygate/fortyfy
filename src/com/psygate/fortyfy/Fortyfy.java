package com.psygate.fortyfy;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.MemorySection;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.psygate.fortyfy.commands.ACommand;
import com.psygate.fortyfy.commands.group.creator.CreateGroupCommand;
import com.psygate.fortyfy.commands.group.creator.DelegateGroupCommand;
import com.psygate.fortyfy.commands.group.creator.DeleteGroupCommand;
import com.psygate.fortyfy.commands.group.creator.RemovePasswordCommand;
import com.psygate.fortyfy.commands.group.creator.RenameGroupCommand;
import com.psygate.fortyfy.commands.group.creator.SetPasswordCommand;
import com.psygate.fortyfy.commands.group.member.JoinGroupCommand;
import com.psygate.fortyfy.commands.group.member.LeaveGroupCommand;
import com.psygate.fortyfy.commands.group.member.ListGroupsCommand;
import com.psygate.fortyfy.commands.group.member.ListOwnedGroupsCommand;
import com.psygate.fortyfy.commands.group.moderator.AddMemberCommand;
import com.psygate.fortyfy.commands.group.moderator.KickMemberCommand;
import com.psygate.fortyfy.commands.group.moderator.ListMembersCommand;
import com.psygate.fortyfy.commands.op.SanityCheckCommand;
import com.psygate.fortyfy.commands.reinforce.BypassReinforcementCommand;
import com.psygate.fortyfy.commands.reinforce.BypassReinforcementStopCommand;
import com.psygate.fortyfy.commands.reinforce.GroupReinforceCommand;
import com.psygate.fortyfy.commands.reinforce.PlaceReinforceCommand;
import com.psygate.fortyfy.commands.reinforce.PublicReinforceCommand;
import com.psygate.fortyfy.commands.reinforce.ReinforcementInfoCommand;
import com.psygate.fortyfy.commands.reinforce.ReinforcementStop;
import com.psygate.fortyfy.database.Database;
import com.psygate.fortyfy.database.daos.IGroupDAO;
import com.psygate.fortyfy.database.daos.IReinforcementDAO;
import com.psygate.fortyfy.database.daos.SimpleGroupDAO;
import com.psygate.fortyfy.database.daos.SimpleReinforcementDAO;
import com.psygate.fortyfy.database.exceptions.UnsupportedDBException;
import com.psygate.fortyfy.database.exceptions.UnsupportedDBPortException;
import com.psygate.fortyfy.datastructuresandvalues.PlayerStateHolder;
import com.psygate.fortyfy.datastructuresandvalues.vectors.Vector2;
import com.psygate.fortyfy.fortification.Reinforcements;
import com.psygate.fortyfy.helpers.chat.ChatHandler;
import com.psygate.fortyfy.helpers.chat.MessageType;
import com.psygate.fortyfy.listeners.BedEnterListener;
import com.psygate.fortyfy.listeners.BlockBreakListener;
import com.psygate.fortyfy.listeners.BlockPlaceListener;
import com.psygate.fortyfy.listeners.BucketPlacementListener;
import com.psygate.fortyfy.listeners.ChestOpenListener;
import com.psygate.fortyfy.listeners.DoorOpenListener;
import com.psygate.fortyfy.listeners.ExplosionListener;
import com.psygate.fortyfy.listeners.FireBreakListener;
import com.psygate.fortyfy.listeners.HelperListener;
import com.psygate.fortyfy.listeners.InfoModeListener;
import com.psygate.fortyfy.listeners.MovingBlockListener;
import com.psygate.fortyfy.listeners.PistonListener;
import com.psygate.fortyfy.listeners.RedstoneListener;
import com.psygate.fortyfy.listeners.LiquidBreakListener;

public class Fortyfy extends JavaPlugin {
	public static final String LOGGERNAME = "Fortyfy";
	private Logger logger = Logger.getLogger(LOGGERNAME);
	private Reinforcements reinforcements = new Reinforcements();
	private PlayerStateHolder ps;
	private HashMap<String, ACommand> coms = new HashMap<String, ACommand>();
	private Database db;
	private static Fortyfy fortyfy;
	private IGroupDAO gdao;
	private IReinforcementDAO rdao;
	private Timer timer = new Timer(true);
	private String name = "Fortyfy";
	private String main = this.getClass().getName();
	private String version = "0.0.2b";
	private String description = "Protection of structures against griefers, usable by simple users.";
	private String author = "psygate";
	private boolean database = false;
	private int redstonedistance = 10;
	private int maxgroupcount = 60;

	public Fortyfy() {
		logger.setLevel(Level.INFO);
	}

	public String getPluginName() {
		return name;
	}

	public String getPluginMain() {
		return main;
	}

	public String getPluginVersion() {
		return version;
	}

	public String getPluginDescription() {
		return description;
	}

	public String getPluginAuthor() {
		return author;
	}

	public boolean isPluginDatabase() {
		return database;
	}

	@Override
	public void onEnable() {
		super.onEnable();
		fortyfy = this;
		logger.setLevel(Level.ALL);
		ps = new PlayerStateHolder(this);
		try {
			parseConfiguration();
			prepareDatabase();
		} catch (UnsupportedDBException e) {
			e.printStackTrace();
			abort();
		} catch (UnsupportedDBPortException e) {
			e.printStackTrace();
			abort();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			abort();
		} catch (SQLException e) {
			e.printStackTrace();
			abort();
		} catch (IOException e) {
			e.printStackTrace();
			abort();
		}

		registerCommands(coms);
		registerListeners();
	}

	@Override
	public void onDisable() {
		super.onDisable();
		try {
			if (db != null)
				db.shutdown();
			timer.cancel();
			BlockPlaceEvent.getHandlerList().unregister(this);
			if (ps != null)
				ps.terminate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void registerCommands(HashMap<String, ACommand> coms) {
		addCommand(new CreateGroupCommand(), coms);
		addCommand(new DelegateGroupCommand(), coms);
		addCommand(new DeleteGroupCommand(), coms);
		addCommand(new RenameGroupCommand(), coms);
		addCommand(new SetPasswordCommand(), coms);
		addCommand(new JoinGroupCommand(), coms);
		addCommand(new LeaveGroupCommand(), coms);
		addCommand(new ListGroupsCommand(), coms);
		addCommand(new ListOwnedGroupsCommand(), coms);
		addCommand(new AddMemberCommand(), coms);
		addCommand(new KickMemberCommand(), coms);
		addCommand(new PlaceReinforceCommand(), coms);
		addCommand(new GroupReinforceCommand(), coms);
		addCommand(new RemovePasswordCommand(), coms);
		addCommand(new BypassReinforcementCommand(), coms);
		addCommand(new BypassReinforcementStopCommand(), coms);
		addCommand(new ListMembersCommand(), coms);
		addCommand(new PublicReinforceCommand(), coms);
		addCommand(new ReinforcementStop(), coms);
		addCommand(new SanityCheckCommand(), coms);
		addCommand(new ReinforcementInfoCommand(), coms);
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(
				new BlockBreakListener(this), this);
		getServer().getPluginManager().registerEvents(
				new BlockPlaceListener(this), this);
		getServer().getPluginManager().registerEvents(
				new ChestOpenListener(this), this);
		getServer().getPluginManager().registerEvents(
				new DoorOpenListener(this), this);
		getServer().getPluginManager().registerEvents(
				new ExplosionListener(this), this);
		// getServer().getPluginManager().registerEvents(new
		// HelperListener(this), this);
		getServer().getPluginManager().registerEvents(
				new RedstoneListener(this), this);
		getServer().getPluginManager().registerEvents(
				new BedEnterListener(this), this);
		getServer().getPluginManager().registerEvents(
				new LiquidBreakListener(this), this);
		getServer().getPluginManager().registerEvents(
				new BucketPlacementListener(this), this);
		getServer().getPluginManager().registerEvents(
				new FireBreakListener(this), this);
		getServer().getPluginManager().registerEvents(new PistonListener(this),
				this);
		getServer().getPluginManager().registerEvents(
				new InfoModeListener(this), this);
		getServer().getPluginManager().registerEvents(
				new MovingBlockListener(this), this);
	}

	private void addCommand(ACommand com, HashMap<String, ACommand> coms) {
		logger.info("Added command \"" + com.getCommandString() + "\"");
		coms.put(com.getCommandString(), com);
	}

	public int getRedstoneDistance() {
		return redstonedistance;
	}

	public void schedule(TimerTask task, long interval) {
		if (getServer() == null) {
			logger.severe("Server null.");
		} else if (getServer().getScheduler() == null) {
			logger.severe("Scheduler null.");
		}

		try {
			getServer().getScheduler().runTaskTimerAsynchronously(this, task,
					interval, interval);
		} catch (Exception e) {
			logger.info("Scheduler error. Using internal timer.");
			timer.schedule(task, 0, interval / 20 * 1000);
		}
	}

	public static void abort() {
	}

	public Reinforcements getReinforcements() {
		return reinforcements;
	}

	public PlayerStateHolder getPlayerStateHolder() {
		return ps;
	}

	@SuppressWarnings("unchecked")
	private void buildMaterialMap() {
		Collection<Map<String, Object>> reinf = (Collection<Map<String, Object>>) getConfig()
				.get("materials");

		for (Map<String, Object> material : reinf) {
			Material mat = Material.getMaterial((String) material.get("name"));
			int strength = (Integer) material.get("strength");
			int required = (Integer) material.get("requirements");
			Material flashermat = Material.getMaterial((String) material
					.get("flasher"));
			reinforcements
					.addFortification(mat, strength, required, flashermat);
			// logger.info("Added Reinforcement: " + mat + "@" + strength +
			// " r:"
			// + required + " f:" + flashermat);
			Collection<String> nreinf = (Collection<String>) getConfig().get(
					"nonReinforceable");
			for (String mats : nreinf) {
				reinforcements.addNonReinforceable(Material.getMaterial(mats));
				// logger.info("Added nonReinforcable: " +
				// Material.getMaterial(mat));
			}
		}
	}

	private void parseDatabaseSettings() throws UnsupportedDBException,
			UnsupportedDBPortException {
		MemorySection section = (MemorySection) getConfig().get(
				"general.database");
		String driver = section.getString("driver");
		String prefix = section.getString("prefix");

		logger.info("Table prefix: " + prefix);

		if (driver.toLowerCase().contains("sqlite")) {
			logger.info("Initializing an sqlite database @" + getDataFolder());
			db = new Database(new File(getDataFolder(), "fortyfy.db"), prefix);
		} else if (driver.toLowerCase().contains("postgres")) {
			String url = section.getString("url") + ":"
					+ section.getString("database");
			String user = section.getString("username");
			String password = section.getString("password");
			logger.info("Initializing a postgres database @" + url);
			db = new Database(Database.DatabaseType.POSTGRES, user, password,
					url, prefix, this);
		} else if (driver.toLowerCase().contains("mysql")) {
			String url = section.getString("url");
			String user = section.getString("username");
			String password = section.getString("password");
			logger.info("Initializing a mysql database @" + url);
			db = new Database(Database.DatabaseType.MYSQL, user, password, url,
					prefix, this);
		} else if (driver.toLowerCase().contains("hsql")) {
			String url = section.getString("url");
			String user = section.getString("username");
			String password = section.getString("password");
			logger.info("Initializing a hsqldb database @" + url);
			db = new Database(Database.DatabaseType.HSQLDB, user, password,
					url, prefix, this);
		} else {
			logger.warning("Unable to determine database type in configuration file. Falling back to sqlite.");
			db = new Database(new File(getDataFolder(), "fortyfy.db"), prefix);
		}

		if (getConfig().getBoolean("general.database.rebuild")) {
			logger.info("Rebuild of database ordered. Data will be erased in the process.");
			db.resetOnStartup();
		}
	}

	private void parseConfiguration() throws UnsupportedDBException,
			UnsupportedDBPortException {
		fortyfy.reloadConfig();
		fortyfy.getConfig().options().copyDefaults(true);
		fortyfy.saveConfig();
		buildMaterialMap();
		parseDatabaseSettings();
		fortyfy.getConfig().getInt("general.groupsAllowed");
		String level = fortyfy.getConfig().getString("general.loglevel");
		Logger.getLogger(LOGGERNAME).setLevel(Level.parse(level));
		redstonedistance = getConfig().getInt("general.redstonedistance");
	}

	private void prepareDatabase() throws ClassNotFoundException, SQLException,
			IOException {
		db.bootStrap();
		gdao = new SimpleGroupDAO(db);
		rdao = new SimpleReinforcementDAO(db, this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		ACommand com = coms.get(command.getName());
		if (com != null) {
			try {
				Vector2<MessageType, String> vec = com.execute(sender, command,
						label, args, db, this);
				if (vec != null)
					ChatHandler.sendMessage(vec.getA(), sender, vec.getB());
			} catch (Exception e) {
				ChatHandler.sendMessage(MessageType.ERROR, sender,
						"There was an error with your request. Sorry");
				logger.log(Level.SEVERE, "ERROR.", e);
			}
			return true;
		}

		return false;
	}

	public IGroupDAO getGroupDAO() {
		return gdao;
	}

	public IReinforcementDAO getReinforcementDAO() {
		return rdao;
	}

	public int getMaxGroupCount() {
		return maxgroupcount;
	}

}
